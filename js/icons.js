"use strict";

(({
  Toolshed,
  theme
}) => {
  /**
   * Loading and rendering iconsets with Javascript.
   */
  Drupal.Iconset = {
    instances: new Map(),
    /**
     * Check and format a new iconset to the static iconset map.
     *
     * @param {array} key
     *   The machine identifier for this iconset.
     * @param {object} data
     *   Raw data object with information on how to render the new data.
     *
     * @return {Map|false}
     *   The processed iconset information in a Map (keyed by icon ID) or
     *   boolean FALSE if unable to build the icon data into a useable format.
     */
    processIconset(key, data) {
      if (this.handlers[data.type]) {
        return this.handlers[data.type](data);
      }
      return false;
    },
    /**
     * Create a new Promise to load an iconset from the iconset ID.
     *
     * @param {string} iconsetId
     *   The machine name of the iconset to load from the server.
     *
     * @return {Promise}
     *   Returns a promise for asynchronously loading the data for building
     *   additional iconsets for the selector to load.
     */
    fetchIconset(iconsetId) {
      return Toolshed.sendRequest(`ajax/iconset/${iconsetId}/info.json`).then(json => {
        const data = typeof json === 'string' || json instanceof String ? JSON.parse(json) : json;
        const iconset = this.processIconset(iconsetId, data);
        this.instances.set(iconsetId, iconset);
        return iconset;
      }, resp => {
        this.iconsets.set(iconsetId, false);
        Error(`${resp.status}: ${resp.text}`);
      });
    },
    /**
     * Check if the iconset has already been loaded, and is already available.
     *
     * this function does not next if iconsets can be loaded, only if they have
     * already been loaded.
     *
     * @param {string} id
     *   Machine name of the iconset check if it is already loaded.
     *
     * @return {bool}
     *   Returns true IFF the iconset has already been loaded to the clientside.
     */
    isLoaded(id) {
      const data = this.instances.get(id);
      return data instanceof Map || data === false;
    },
    /**
     * Get loaded icon data if it is already available.
     *
     * @param {string} id
     *   Machine name of the iconset check to return if loaded.
     *
     * @return {array|false|null}
     *   Returns an array of the iconset icon data if a valid iconset is already
     *   loaded and processed. Will return boolean FALSE, if the iconset ID is
     *   invalid, or fails to load. NULL is returned if the data is not loaded
     *   or is in the progress of being requested.
     */
    getLoaded(id) {
      const data = this.instances.get(id);
      return data instanceof Map || data === false ? data : null;
    },
    /**
     * Fetchs the iconset data asynchronously if not already loaded. This method
     * handles sets which already are loaded, and sets that need to be loaded.
     *
     * Because the return is wrapped in a Promise even if the data is already
     * loaded and cached, there are circumstances where you may want to use
     * isLoaded() and getLoaded() to avoid FOUC or DOM manipulation issues.
     *
     * @param {string} id
     *   Machine name of the iconset to fetch, or return as the promise data.
     *
     * @return {Promise}
     *   Returns true IFF the iconset has already been loaded to the clientside.
     */
    getAsync(id) {
      const data = this.instances.get(id);
      if (data instanceof Map || data === false) {
        return Promise.resolve(data);
      }
      let promise = data;
      if (!data || typeof data.then !== 'function') {
        promise = this.fetchIconset(id);
        this.instances.set(id, promise);
      }
      return promise;
    }
  };

  /**
   * Iconset plugin handlers can register handlers to be used to process the
   * plugin handler's JSON data format into consistent icon data. This allows
   * different plugins to have different data formats and utilize different
   * theme functions in Javascript.
   *
   * All handlers are functions, that take information about a loaded asset,
   * as a JSON object, and return an array of icon object, each consisting of:
   *  - icon ID
   *  - label
   *  - string of HTML to preview the icon.
   */
  Drupal.Iconset.handlers = {
    /**
     * Function for converting the data from the iconset.
     *
     * @param {Object} data
     *   Iconset data about the iconset icons. At a minimum this data should
     *   have a "type" and "assets" properties. Assets is where information
     *   about the individual icons are contained.
     *
     * @return {Map}
     *   The Map object which was populated with the icons.
     */
    svg(data) {
      const icons = new Map();
      data.assets.forEach(asset => {
        const renderer = asset.type === 'sprite' ? theme.iconsetSvgSpriteItem : theme.iconsetImage;
        asset.icons.forEach(icon => {
          icons.set(icon.id, {
            id: icon.id,
            label: icon.label,
            html: renderer({
              ...icon,
              file: asset.file
            })
          });
        });
      });
      return icons;
    },
    /**
     * Function for converting font icon data to renderable icons.
     *
     * @param {Object} data
     *   Data about a font icon, including the font properties, and individual
     *   data about font assets, and a map of unicode characters to icon IDs.
     *
     * @return {Map}
     *   The Map object which was populated with the icons.
     */
    font_icon(data) {
      const icons = new Map();
      data.assets.forEach(asset => {
        const {
          tag
        } = data;
        const className = data.className.reduce((acc, curr) => `${acc} ${curr}`, '');
        asset.icons.forEach(icon => {
          icons.set(icon.id, {
            id: icon.id,
            label: icon.label,
            html: theme.iconsetFontIcon(icon, tag, className)
          });
        });
      });
      return icons;
    },
    /**
     * Function for converting image file asset data to renderable icons.
     *
     * @param {Object} data
     *   Data about a font icon, including individual image files, dimensions,
     *   the image ID and label.
     *
     * @return {Map}
     *   The Map object which was populated with the icons.
     */
    image(data) {
      const icons = new Map();
      data.assets.forEach(icon => {
        icons.set(icon.id, {
          id: icon.id,
          label: icon.label,
          html: theme.iconsetImage(icon)
        });
      });
      return icons;
    }
  };

  /**
   * Theme function to render SVG icon data from a sprite asset.
   *
   * @param {object} icon
   *   Information about the icon to render.
   * @param {object} opts
   *   Object with additional options like width and height to apply to the
   *   rendering of the icon.
   *
   * @return {string}
   *   The HTML content to use with as the icon display.
   */
  theme.iconsetSvgSpriteItem = (icon, opts = {}) => {
    let attrs = `class="icon icon--svg" alt="${icon.label}"`;
    if (icon.viewBox) {
      attrs += ` viewBox="${icon.viewBox}"`;
    }
    if (opts.height) {
      attrs += ` height="${opts.height}"`;
    }
    if (opts.width) {
      attrs += ` width="${opts.width}"`;
    }
    return `<svg ${attrs}>
      <use xlink:href="${encodeURI(icon.file)}#${encodeURIComponent(icon.id)}"/>
    </svg>`;
  };

  /**
   * Theme function to render image file into an iconset icon.
   *
   * @param {object} icon
   *   Information about the icon to render.
   * @param {object} opts
   *   Object with additional options like width and height to apply to the
   *   rendering of the icon.
   *
   * @return {string}
   *   The HTML content to use with as the icon display.
   */
  theme.iconsetImage = (icon, opts = {}) => {
    const height = opts.height || icon.height;
    const width = opts.width || icon.width;
    let attrs = `class="icon icon--img" alt="${icon.label}"`;
    if (height) {
      attrs += ` height="${height}"`;
    }
    if (width) {
      attrs += ` width="${width}"`;
    }
    return `<img ${attrs} src="${encodeURI(icon.file)}"/>`;
  };

  /**
   * Theme function to render SVG icon data from iconset.
   *
   * @param {object} icon
   *   Information about the icon to render.
   * @param {string} tag
   *   The HTML tag to use as the icon element.
   * @param {string} className
   *   CSS class name string to apply for the font icon element wrapper. Most
   *   font based icons use css class names to apply the font styles so that
   *   the content renders using the correct font-face and styles.
   * @param {object} opts
   *   Object with additional options like width and height to apply to the
   *   rendering of the icon.
   *
   * @return {string}
   *   The HTML content to use with as the icon display.
   */
  theme.iconsetFontIcon = (icon, tag, className = '', opts = {}) => {
    let attrs = `class="${`${className} icon icon--font`.trim()}" role="img" alt="${icon.label}"`;
    const dim = opts.height || opts.width;
    if (dim) {
      attrs += ` style="font-size: ${dim}px;"`;
    }
    return `<${tag} ${attrs}>${decodeURIComponent(icon.unicode)}</${tag}>`;
  };
})(Drupal);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaWNvbnMuanMiLCJuYW1lcyI6WyJUb29sc2hlZCIsInRoZW1lIiwiRHJ1cGFsIiwiSWNvbnNldCIsImluc3RhbmNlcyIsIk1hcCIsInByb2Nlc3NJY29uc2V0Iiwia2V5IiwiZGF0YSIsImhhbmRsZXJzIiwidHlwZSIsImZldGNoSWNvbnNldCIsImljb25zZXRJZCIsInNlbmRSZXF1ZXN0IiwidGhlbiIsImpzb24iLCJTdHJpbmciLCJKU09OIiwicGFyc2UiLCJpY29uc2V0Iiwic2V0IiwicmVzcCIsImljb25zZXRzIiwiRXJyb3IiLCJzdGF0dXMiLCJ0ZXh0IiwiaXNMb2FkZWQiLCJpZCIsImdldCIsImdldExvYWRlZCIsImdldEFzeW5jIiwiUHJvbWlzZSIsInJlc29sdmUiLCJwcm9taXNlIiwic3ZnIiwiaWNvbnMiLCJhc3NldHMiLCJmb3JFYWNoIiwiYXNzZXQiLCJyZW5kZXJlciIsImljb25zZXRTdmdTcHJpdGVJdGVtIiwiaWNvbnNldEltYWdlIiwiaWNvbiIsImxhYmVsIiwiaHRtbCIsImZpbGUiLCJmb250X2ljb24iLCJ0YWciLCJjbGFzc05hbWUiLCJyZWR1Y2UiLCJhY2MiLCJjdXJyIiwiaWNvbnNldEZvbnRJY29uIiwiaW1hZ2UiLCJvcHRzIiwiYXR0cnMiLCJ2aWV3Qm94IiwiaGVpZ2h0Iiwid2lkdGgiLCJlbmNvZGVVUkkiLCJlbmNvZGVVUklDb21wb25lbnQiLCJ0cmltIiwiZGltIiwiZGVjb2RlVVJJQ29tcG9uZW50IiwidW5pY29kZSJdLCJzb3VyY2VzIjpbImljb25zLmVzNi5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIoKHsgVG9vbHNoZWQsIHRoZW1lIH0pID0+IHtcbiAgLyoqXG4gICAqIExvYWRpbmcgYW5kIHJlbmRlcmluZyBpY29uc2V0cyB3aXRoIEphdmFzY3JpcHQuXG4gICAqL1xuICBEcnVwYWwuSWNvbnNldCA9IHtcbiAgICBpbnN0YW5jZXM6IG5ldyBNYXAoKSxcblxuICAgIC8qKlxuICAgICAqIENoZWNrIGFuZCBmb3JtYXQgYSBuZXcgaWNvbnNldCB0byB0aGUgc3RhdGljIGljb25zZXQgbWFwLlxuICAgICAqXG4gICAgICogQHBhcmFtIHthcnJheX0ga2V5XG4gICAgICogICBUaGUgbWFjaGluZSBpZGVudGlmaWVyIGZvciB0aGlzIGljb25zZXQuXG4gICAgICogQHBhcmFtIHtvYmplY3R9IGRhdGFcbiAgICAgKiAgIFJhdyBkYXRhIG9iamVjdCB3aXRoIGluZm9ybWF0aW9uIG9uIGhvdyB0byByZW5kZXIgdGhlIG5ldyBkYXRhLlxuICAgICAqXG4gICAgICogQHJldHVybiB7TWFwfGZhbHNlfVxuICAgICAqICAgVGhlIHByb2Nlc3NlZCBpY29uc2V0IGluZm9ybWF0aW9uIGluIGEgTWFwIChrZXllZCBieSBpY29uIElEKSBvclxuICAgICAqICAgYm9vbGVhbiBGQUxTRSBpZiB1bmFibGUgdG8gYnVpbGQgdGhlIGljb24gZGF0YSBpbnRvIGEgdXNlYWJsZSBmb3JtYXQuXG4gICAgICovXG4gICAgcHJvY2Vzc0ljb25zZXQoa2V5LCBkYXRhKSB7XG4gICAgICBpZiAodGhpcy5oYW5kbGVyc1tkYXRhLnR5cGVdKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmhhbmRsZXJzW2RhdGEudHlwZV0oZGF0YSk7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogQ3JlYXRlIGEgbmV3IFByb21pc2UgdG8gbG9hZCBhbiBpY29uc2V0IGZyb20gdGhlIGljb25zZXQgSUQuXG4gICAgICpcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gaWNvbnNldElkXG4gICAgICogICBUaGUgbWFjaGluZSBuYW1lIG9mIHRoZSBpY29uc2V0IHRvIGxvYWQgZnJvbSB0aGUgc2VydmVyLlxuICAgICAqXG4gICAgICogQHJldHVybiB7UHJvbWlzZX1cbiAgICAgKiAgIFJldHVybnMgYSBwcm9taXNlIGZvciBhc3luY2hyb25vdXNseSBsb2FkaW5nIHRoZSBkYXRhIGZvciBidWlsZGluZ1xuICAgICAqICAgYWRkaXRpb25hbCBpY29uc2V0cyBmb3IgdGhlIHNlbGVjdG9yIHRvIGxvYWQuXG4gICAgICovXG4gICAgZmV0Y2hJY29uc2V0KGljb25zZXRJZCkge1xuICAgICAgcmV0dXJuIFRvb2xzaGVkLnNlbmRSZXF1ZXN0KGBhamF4L2ljb25zZXQvJHtpY29uc2V0SWR9L2luZm8uanNvbmApLnRoZW4oXG4gICAgICAgIChqc29uKSA9PiB7XG4gICAgICAgICAgY29uc3QgZGF0YSA9ICh0eXBlb2YganNvbiA9PT0gJ3N0cmluZycgfHwganNvbiBpbnN0YW5jZW9mIFN0cmluZykgPyBKU09OLnBhcnNlKGpzb24pIDoganNvbjtcbiAgICAgICAgICBjb25zdCBpY29uc2V0ID0gdGhpcy5wcm9jZXNzSWNvbnNldChpY29uc2V0SWQsIGRhdGEpO1xuXG4gICAgICAgICAgdGhpcy5pbnN0YW5jZXMuc2V0KGljb25zZXRJZCwgaWNvbnNldCk7XG4gICAgICAgICAgcmV0dXJuIGljb25zZXQ7XG4gICAgICAgIH0sXG4gICAgICAgIChyZXNwKSA9PiB7XG4gICAgICAgICAgdGhpcy5pY29uc2V0cy5zZXQoaWNvbnNldElkLCBmYWxzZSk7XG4gICAgICAgICAgRXJyb3IoYCR7cmVzcC5zdGF0dXN9OiAke3Jlc3AudGV4dH1gKTtcbiAgICAgICAgfSxcbiAgICAgICk7XG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIENoZWNrIGlmIHRoZSBpY29uc2V0IGhhcyBhbHJlYWR5IGJlZW4gbG9hZGVkLCBhbmQgaXMgYWxyZWFkeSBhdmFpbGFibGUuXG4gICAgICpcbiAgICAgKiB0aGlzIGZ1bmN0aW9uIGRvZXMgbm90IG5leHQgaWYgaWNvbnNldHMgY2FuIGJlIGxvYWRlZCwgb25seSBpZiB0aGV5IGhhdmVcbiAgICAgKiBhbHJlYWR5IGJlZW4gbG9hZGVkLlxuICAgICAqXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IGlkXG4gICAgICogICBNYWNoaW5lIG5hbWUgb2YgdGhlIGljb25zZXQgY2hlY2sgaWYgaXQgaXMgYWxyZWFkeSBsb2FkZWQuXG4gICAgICpcbiAgICAgKiBAcmV0dXJuIHtib29sfVxuICAgICAqICAgUmV0dXJucyB0cnVlIElGRiB0aGUgaWNvbnNldCBoYXMgYWxyZWFkeSBiZWVuIGxvYWRlZCB0byB0aGUgY2xpZW50c2lkZS5cbiAgICAgKi9cbiAgICBpc0xvYWRlZChpZCkge1xuICAgICAgY29uc3QgZGF0YSA9IHRoaXMuaW5zdGFuY2VzLmdldChpZCk7XG4gICAgICByZXR1cm4gKGRhdGEgaW5zdGFuY2VvZiBNYXAgfHwgZGF0YSA9PT0gZmFsc2UpO1xuICAgIH0sXG5cbiAgICAvKipcbiAgICAgKiBHZXQgbG9hZGVkIGljb24gZGF0YSBpZiBpdCBpcyBhbHJlYWR5IGF2YWlsYWJsZS5cbiAgICAgKlxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBpZFxuICAgICAqICAgTWFjaGluZSBuYW1lIG9mIHRoZSBpY29uc2V0IGNoZWNrIHRvIHJldHVybiBpZiBsb2FkZWQuXG4gICAgICpcbiAgICAgKiBAcmV0dXJuIHthcnJheXxmYWxzZXxudWxsfVxuICAgICAqICAgUmV0dXJucyBhbiBhcnJheSBvZiB0aGUgaWNvbnNldCBpY29uIGRhdGEgaWYgYSB2YWxpZCBpY29uc2V0IGlzIGFscmVhZHlcbiAgICAgKiAgIGxvYWRlZCBhbmQgcHJvY2Vzc2VkLiBXaWxsIHJldHVybiBib29sZWFuIEZBTFNFLCBpZiB0aGUgaWNvbnNldCBJRCBpc1xuICAgICAqICAgaW52YWxpZCwgb3IgZmFpbHMgdG8gbG9hZC4gTlVMTCBpcyByZXR1cm5lZCBpZiB0aGUgZGF0YSBpcyBub3QgbG9hZGVkXG4gICAgICogICBvciBpcyBpbiB0aGUgcHJvZ3Jlc3Mgb2YgYmVpbmcgcmVxdWVzdGVkLlxuICAgICAqL1xuICAgIGdldExvYWRlZChpZCkge1xuICAgICAgY29uc3QgZGF0YSA9IHRoaXMuaW5zdGFuY2VzLmdldChpZCk7XG4gICAgICByZXR1cm4gKGRhdGEgaW5zdGFuY2VvZiBNYXAgfHwgZGF0YSA9PT0gZmFsc2UpID8gZGF0YSA6IG51bGw7XG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIEZldGNocyB0aGUgaWNvbnNldCBkYXRhIGFzeW5jaHJvbm91c2x5IGlmIG5vdCBhbHJlYWR5IGxvYWRlZC4gVGhpcyBtZXRob2RcbiAgICAgKiBoYW5kbGVzIHNldHMgd2hpY2ggYWxyZWFkeSBhcmUgbG9hZGVkLCBhbmQgc2V0cyB0aGF0IG5lZWQgdG8gYmUgbG9hZGVkLlxuICAgICAqXG4gICAgICogQmVjYXVzZSB0aGUgcmV0dXJuIGlzIHdyYXBwZWQgaW4gYSBQcm9taXNlIGV2ZW4gaWYgdGhlIGRhdGEgaXMgYWxyZWFkeVxuICAgICAqIGxvYWRlZCBhbmQgY2FjaGVkLCB0aGVyZSBhcmUgY2lyY3Vtc3RhbmNlcyB3aGVyZSB5b3UgbWF5IHdhbnQgdG8gdXNlXG4gICAgICogaXNMb2FkZWQoKSBhbmQgZ2V0TG9hZGVkKCkgdG8gYXZvaWQgRk9VQyBvciBET00gbWFuaXB1bGF0aW9uIGlzc3Vlcy5cbiAgICAgKlxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBpZFxuICAgICAqICAgTWFjaGluZSBuYW1lIG9mIHRoZSBpY29uc2V0IHRvIGZldGNoLCBvciByZXR1cm4gYXMgdGhlIHByb21pc2UgZGF0YS5cbiAgICAgKlxuICAgICAqIEByZXR1cm4ge1Byb21pc2V9XG4gICAgICogICBSZXR1cm5zIHRydWUgSUZGIHRoZSBpY29uc2V0IGhhcyBhbHJlYWR5IGJlZW4gbG9hZGVkIHRvIHRoZSBjbGllbnRzaWRlLlxuICAgICAqL1xuICAgIGdldEFzeW5jKGlkKSB7XG4gICAgICBjb25zdCBkYXRhID0gdGhpcy5pbnN0YW5jZXMuZ2V0KGlkKTtcblxuICAgICAgaWYgKGRhdGEgaW5zdGFuY2VvZiBNYXAgfHwgZGF0YSA9PT0gZmFsc2UpIHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShkYXRhKTtcbiAgICAgIH1cblxuICAgICAgbGV0IHByb21pc2UgPSBkYXRhO1xuXG4gICAgICBpZiAoIWRhdGEgfHwgdHlwZW9mIGRhdGEudGhlbiAhPT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICBwcm9taXNlID0gdGhpcy5mZXRjaEljb25zZXQoaWQpO1xuICAgICAgICB0aGlzLmluc3RhbmNlcy5zZXQoaWQsIHByb21pc2UpO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gcHJvbWlzZTtcbiAgICB9LFxuICB9O1xuXG4gIC8qKlxuICAgKiBJY29uc2V0IHBsdWdpbiBoYW5kbGVycyBjYW4gcmVnaXN0ZXIgaGFuZGxlcnMgdG8gYmUgdXNlZCB0byBwcm9jZXNzIHRoZVxuICAgKiBwbHVnaW4gaGFuZGxlcidzIEpTT04gZGF0YSBmb3JtYXQgaW50byBjb25zaXN0ZW50IGljb24gZGF0YS4gVGhpcyBhbGxvd3NcbiAgICogZGlmZmVyZW50IHBsdWdpbnMgdG8gaGF2ZSBkaWZmZXJlbnQgZGF0YSBmb3JtYXRzIGFuZCB1dGlsaXplIGRpZmZlcmVudFxuICAgKiB0aGVtZSBmdW5jdGlvbnMgaW4gSmF2YXNjcmlwdC5cbiAgICpcbiAgICogQWxsIGhhbmRsZXJzIGFyZSBmdW5jdGlvbnMsIHRoYXQgdGFrZSBpbmZvcm1hdGlvbiBhYm91dCBhIGxvYWRlZCBhc3NldCxcbiAgICogYXMgYSBKU09OIG9iamVjdCwgYW5kIHJldHVybiBhbiBhcnJheSBvZiBpY29uIG9iamVjdCwgZWFjaCBjb25zaXN0aW5nIG9mOlxuICAgKiAgLSBpY29uIElEXG4gICAqICAtIGxhYmVsXG4gICAqICAtIHN0cmluZyBvZiBIVE1MIHRvIHByZXZpZXcgdGhlIGljb24uXG4gICAqL1xuICBEcnVwYWwuSWNvbnNldC5oYW5kbGVycyA9IHtcblxuICAgIC8qKlxuICAgICAqIEZ1bmN0aW9uIGZvciBjb252ZXJ0aW5nIHRoZSBkYXRhIGZyb20gdGhlIGljb25zZXQuXG4gICAgICpcbiAgICAgKiBAcGFyYW0ge09iamVjdH0gZGF0YVxuICAgICAqICAgSWNvbnNldCBkYXRhIGFib3V0IHRoZSBpY29uc2V0IGljb25zLiBBdCBhIG1pbmltdW0gdGhpcyBkYXRhIHNob3VsZFxuICAgICAqICAgaGF2ZSBhIFwidHlwZVwiIGFuZCBcImFzc2V0c1wiIHByb3BlcnRpZXMuIEFzc2V0cyBpcyB3aGVyZSBpbmZvcm1hdGlvblxuICAgICAqICAgYWJvdXQgdGhlIGluZGl2aWR1YWwgaWNvbnMgYXJlIGNvbnRhaW5lZC5cbiAgICAgKlxuICAgICAqIEByZXR1cm4ge01hcH1cbiAgICAgKiAgIFRoZSBNYXAgb2JqZWN0IHdoaWNoIHdhcyBwb3B1bGF0ZWQgd2l0aCB0aGUgaWNvbnMuXG4gICAgICovXG4gICAgc3ZnKGRhdGEpIHtcbiAgICAgIGNvbnN0IGljb25zID0gbmV3IE1hcCgpO1xuXG4gICAgICBkYXRhLmFzc2V0cy5mb3JFYWNoKChhc3NldCkgPT4ge1xuICAgICAgICBjb25zdCByZW5kZXJlciA9IChhc3NldC50eXBlID09PSAnc3ByaXRlJylcbiAgICAgICAgICA/IHRoZW1lLmljb25zZXRTdmdTcHJpdGVJdGVtXG4gICAgICAgICAgOiB0aGVtZS5pY29uc2V0SW1hZ2U7XG5cbiAgICAgICAgYXNzZXQuaWNvbnMuZm9yRWFjaCgoaWNvbikgPT4ge1xuICAgICAgICAgIGljb25zLnNldChpY29uLmlkLCB7XG4gICAgICAgICAgICBpZDogaWNvbi5pZCxcbiAgICAgICAgICAgIGxhYmVsOiBpY29uLmxhYmVsLFxuICAgICAgICAgICAgaHRtbDogcmVuZGVyZXIoeyAuLi5pY29uLCBmaWxlOiBhc3NldC5maWxlIH0pLFxuICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICAgIH0pO1xuXG4gICAgICByZXR1cm4gaWNvbnM7XG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIEZ1bmN0aW9uIGZvciBjb252ZXJ0aW5nIGZvbnQgaWNvbiBkYXRhIHRvIHJlbmRlcmFibGUgaWNvbnMuXG4gICAgICpcbiAgICAgKiBAcGFyYW0ge09iamVjdH0gZGF0YVxuICAgICAqICAgRGF0YSBhYm91dCBhIGZvbnQgaWNvbiwgaW5jbHVkaW5nIHRoZSBmb250IHByb3BlcnRpZXMsIGFuZCBpbmRpdmlkdWFsXG4gICAgICogICBkYXRhIGFib3V0IGZvbnQgYXNzZXRzLCBhbmQgYSBtYXAgb2YgdW5pY29kZSBjaGFyYWN0ZXJzIHRvIGljb24gSURzLlxuICAgICAqXG4gICAgICogQHJldHVybiB7TWFwfVxuICAgICAqICAgVGhlIE1hcCBvYmplY3Qgd2hpY2ggd2FzIHBvcHVsYXRlZCB3aXRoIHRoZSBpY29ucy5cbiAgICAgKi9cbiAgICBmb250X2ljb24oZGF0YSkge1xuICAgICAgY29uc3QgaWNvbnMgPSBuZXcgTWFwKCk7XG5cbiAgICAgIGRhdGEuYXNzZXRzLmZvckVhY2goKGFzc2V0KSA9PiB7XG4gICAgICAgIGNvbnN0IHsgdGFnIH0gPSBkYXRhO1xuICAgICAgICBjb25zdCBjbGFzc05hbWUgPSBkYXRhLmNsYXNzTmFtZS5yZWR1Y2UoKGFjYywgY3VycikgPT4gYCR7YWNjfSAke2N1cnJ9YCwgJycpO1xuXG4gICAgICAgIGFzc2V0Lmljb25zLmZvckVhY2goKGljb24pID0+IHtcbiAgICAgICAgICBpY29ucy5zZXQoaWNvbi5pZCwge1xuICAgICAgICAgICAgaWQ6IGljb24uaWQsXG4gICAgICAgICAgICBsYWJlbDogaWNvbi5sYWJlbCxcbiAgICAgICAgICAgIGh0bWw6IHRoZW1lLmljb25zZXRGb250SWNvbihpY29uLCB0YWcsIGNsYXNzTmFtZSksXG4gICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgICAgfSk7XG5cbiAgICAgIHJldHVybiBpY29ucztcbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogRnVuY3Rpb24gZm9yIGNvbnZlcnRpbmcgaW1hZ2UgZmlsZSBhc3NldCBkYXRhIHRvIHJlbmRlcmFibGUgaWNvbnMuXG4gICAgICpcbiAgICAgKiBAcGFyYW0ge09iamVjdH0gZGF0YVxuICAgICAqICAgRGF0YSBhYm91dCBhIGZvbnQgaWNvbiwgaW5jbHVkaW5nIGluZGl2aWR1YWwgaW1hZ2UgZmlsZXMsIGRpbWVuc2lvbnMsXG4gICAgICogICB0aGUgaW1hZ2UgSUQgYW5kIGxhYmVsLlxuICAgICAqXG4gICAgICogQHJldHVybiB7TWFwfVxuICAgICAqICAgVGhlIE1hcCBvYmplY3Qgd2hpY2ggd2FzIHBvcHVsYXRlZCB3aXRoIHRoZSBpY29ucy5cbiAgICAgKi9cbiAgICBpbWFnZShkYXRhKSB7XG4gICAgICBjb25zdCBpY29ucyA9IG5ldyBNYXAoKTtcblxuICAgICAgZGF0YS5hc3NldHMuZm9yRWFjaCgoaWNvbikgPT4ge1xuICAgICAgICBpY29ucy5zZXQoaWNvbi5pZCwge1xuICAgICAgICAgIGlkOiBpY29uLmlkLFxuICAgICAgICAgIGxhYmVsOiBpY29uLmxhYmVsLFxuICAgICAgICAgIGh0bWw6IHRoZW1lLmljb25zZXRJbWFnZShpY29uKSxcbiAgICAgICAgfSk7XG4gICAgICB9KTtcblxuICAgICAgcmV0dXJuIGljb25zO1xuICAgIH0sXG4gIH07XG5cbiAgLyoqXG4gICAqIFRoZW1lIGZ1bmN0aW9uIHRvIHJlbmRlciBTVkcgaWNvbiBkYXRhIGZyb20gYSBzcHJpdGUgYXNzZXQuXG4gICAqXG4gICAqIEBwYXJhbSB7b2JqZWN0fSBpY29uXG4gICAqICAgSW5mb3JtYXRpb24gYWJvdXQgdGhlIGljb24gdG8gcmVuZGVyLlxuICAgKiBAcGFyYW0ge29iamVjdH0gb3B0c1xuICAgKiAgIE9iamVjdCB3aXRoIGFkZGl0aW9uYWwgb3B0aW9ucyBsaWtlIHdpZHRoIGFuZCBoZWlnaHQgdG8gYXBwbHkgdG8gdGhlXG4gICAqICAgcmVuZGVyaW5nIG9mIHRoZSBpY29uLlxuICAgKlxuICAgKiBAcmV0dXJuIHtzdHJpbmd9XG4gICAqICAgVGhlIEhUTUwgY29udGVudCB0byB1c2Ugd2l0aCBhcyB0aGUgaWNvbiBkaXNwbGF5LlxuICAgKi9cbiAgdGhlbWUuaWNvbnNldFN2Z1Nwcml0ZUl0ZW0gPSAoaWNvbiwgb3B0cyA9IHt9KSA9PiB7XG4gICAgbGV0IGF0dHJzID0gYGNsYXNzPVwiaWNvbiBpY29uLS1zdmdcIiBhbHQ9XCIke2ljb24ubGFiZWx9XCJgO1xuXG4gICAgaWYgKGljb24udmlld0JveCkge1xuICAgICAgYXR0cnMgKz0gYCB2aWV3Qm94PVwiJHtpY29uLnZpZXdCb3h9XCJgO1xuICAgIH1cblxuICAgIGlmIChvcHRzLmhlaWdodCkge1xuICAgICAgYXR0cnMgKz0gYCBoZWlnaHQ9XCIke29wdHMuaGVpZ2h0fVwiYDtcbiAgICB9XG4gICAgaWYgKG9wdHMud2lkdGgpIHtcbiAgICAgIGF0dHJzICs9IGAgd2lkdGg9XCIke29wdHMud2lkdGh9XCJgO1xuICAgIH1cblxuICAgIHJldHVybiBgPHN2ZyAke2F0dHJzfT5cbiAgICAgIDx1c2UgeGxpbms6aHJlZj1cIiR7ZW5jb2RlVVJJKGljb24uZmlsZSl9IyR7ZW5jb2RlVVJJQ29tcG9uZW50KGljb24uaWQpfVwiLz5cbiAgICA8L3N2Zz5gO1xuICB9O1xuXG4gIC8qKlxuICAgKiBUaGVtZSBmdW5jdGlvbiB0byByZW5kZXIgaW1hZ2UgZmlsZSBpbnRvIGFuIGljb25zZXQgaWNvbi5cbiAgICpcbiAgICogQHBhcmFtIHtvYmplY3R9IGljb25cbiAgICogICBJbmZvcm1hdGlvbiBhYm91dCB0aGUgaWNvbiB0byByZW5kZXIuXG4gICAqIEBwYXJhbSB7b2JqZWN0fSBvcHRzXG4gICAqICAgT2JqZWN0IHdpdGggYWRkaXRpb25hbCBvcHRpb25zIGxpa2Ugd2lkdGggYW5kIGhlaWdodCB0byBhcHBseSB0byB0aGVcbiAgICogICByZW5kZXJpbmcgb2YgdGhlIGljb24uXG4gICAqXG4gICAqIEByZXR1cm4ge3N0cmluZ31cbiAgICogICBUaGUgSFRNTCBjb250ZW50IHRvIHVzZSB3aXRoIGFzIHRoZSBpY29uIGRpc3BsYXkuXG4gICAqL1xuICB0aGVtZS5pY29uc2V0SW1hZ2UgPSAoaWNvbiwgb3B0cyA9IHt9KSA9PiB7XG4gICAgY29uc3QgaGVpZ2h0ID0gb3B0cy5oZWlnaHQgfHwgaWNvbi5oZWlnaHQ7XG4gICAgY29uc3Qgd2lkdGggPSBvcHRzLndpZHRoIHx8IGljb24ud2lkdGg7XG5cbiAgICBsZXQgYXR0cnMgPSBgY2xhc3M9XCJpY29uIGljb24tLWltZ1wiIGFsdD1cIiR7aWNvbi5sYWJlbH1cImA7XG5cbiAgICBpZiAoaGVpZ2h0KSB7XG4gICAgICBhdHRycyArPSBgIGhlaWdodD1cIiR7aGVpZ2h0fVwiYDtcbiAgICB9XG4gICAgaWYgKHdpZHRoKSB7XG4gICAgICBhdHRycyArPSBgIHdpZHRoPVwiJHt3aWR0aH1cImA7XG4gICAgfVxuXG4gICAgcmV0dXJuIGA8aW1nICR7YXR0cnN9IHNyYz1cIiR7ZW5jb2RlVVJJKGljb24uZmlsZSl9XCIvPmA7XG4gIH07XG5cbiAgLyoqXG4gICAqIFRoZW1lIGZ1bmN0aW9uIHRvIHJlbmRlciBTVkcgaWNvbiBkYXRhIGZyb20gaWNvbnNldC5cbiAgICpcbiAgICogQHBhcmFtIHtvYmplY3R9IGljb25cbiAgICogICBJbmZvcm1hdGlvbiBhYm91dCB0aGUgaWNvbiB0byByZW5kZXIuXG4gICAqIEBwYXJhbSB7c3RyaW5nfSB0YWdcbiAgICogICBUaGUgSFRNTCB0YWcgdG8gdXNlIGFzIHRoZSBpY29uIGVsZW1lbnQuXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBjbGFzc05hbWVcbiAgICogICBDU1MgY2xhc3MgbmFtZSBzdHJpbmcgdG8gYXBwbHkgZm9yIHRoZSBmb250IGljb24gZWxlbWVudCB3cmFwcGVyLiBNb3N0XG4gICAqICAgZm9udCBiYXNlZCBpY29ucyB1c2UgY3NzIGNsYXNzIG5hbWVzIHRvIGFwcGx5IHRoZSBmb250IHN0eWxlcyBzbyB0aGF0XG4gICAqICAgdGhlIGNvbnRlbnQgcmVuZGVycyB1c2luZyB0aGUgY29ycmVjdCBmb250LWZhY2UgYW5kIHN0eWxlcy5cbiAgICogQHBhcmFtIHtvYmplY3R9IG9wdHNcbiAgICogICBPYmplY3Qgd2l0aCBhZGRpdGlvbmFsIG9wdGlvbnMgbGlrZSB3aWR0aCBhbmQgaGVpZ2h0IHRvIGFwcGx5IHRvIHRoZVxuICAgKiAgIHJlbmRlcmluZyBvZiB0aGUgaWNvbi5cbiAgICpcbiAgICogQHJldHVybiB7c3RyaW5nfVxuICAgKiAgIFRoZSBIVE1MIGNvbnRlbnQgdG8gdXNlIHdpdGggYXMgdGhlIGljb24gZGlzcGxheS5cbiAgICovXG4gIHRoZW1lLmljb25zZXRGb250SWNvbiA9IChpY29uLCB0YWcsIGNsYXNzTmFtZSA9ICcnLCBvcHRzID0ge30pID0+IHtcbiAgICBsZXQgYXR0cnMgPSBgY2xhc3M9XCIke2Ake2NsYXNzTmFtZX0gaWNvbiBpY29uLS1mb250YC50cmltKCl9XCIgcm9sZT1cImltZ1wiIGFsdD1cIiR7aWNvbi5sYWJlbH1cImA7XG5cbiAgICBjb25zdCBkaW0gPSBvcHRzLmhlaWdodCB8fCBvcHRzLndpZHRoO1xuICAgIGlmIChkaW0pIHtcbiAgICAgIGF0dHJzICs9IGAgc3R5bGU9XCJmb250LXNpemU6ICR7ZGltfXB4O1wiYDtcbiAgICB9XG5cbiAgICByZXR1cm4gYDwke3RhZ30gJHthdHRyc30+JHtkZWNvZGVVUklDb21wb25lbnQoaWNvbi51bmljb2RlKX08LyR7dGFnfT5gO1xuICB9O1xufSkoRHJ1cGFsKTtcbiJdLCJtYXBwaW5ncyI6Ijs7QUFBQSxDQUFDLENBQUM7RUFBRUEsUUFBUTtFQUFFQztBQUFNLENBQUMsS0FBSztFQUN4QjtBQUNGO0FBQ0E7RUFDRUMsTUFBTSxDQUFDQyxPQUFPLEdBQUc7SUFDZkMsU0FBUyxFQUFFLElBQUlDLEdBQUcsRUFBRTtJQUVwQjtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7SUFDSUMsY0FBYyxDQUFDQyxHQUFHLEVBQUVDLElBQUksRUFBRTtNQUN4QixJQUFJLElBQUksQ0FBQ0MsUUFBUSxDQUFDRCxJQUFJLENBQUNFLElBQUksQ0FBQyxFQUFFO1FBQzVCLE9BQU8sSUFBSSxDQUFDRCxRQUFRLENBQUNELElBQUksQ0FBQ0UsSUFBSSxDQUFDLENBQUNGLElBQUksQ0FBQztNQUN2QztNQUVBLE9BQU8sS0FBSztJQUNkLENBQUM7SUFFRDtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtJQUNJRyxZQUFZLENBQUNDLFNBQVMsRUFBRTtNQUN0QixPQUFPWixRQUFRLENBQUNhLFdBQVcsQ0FBRSxnQkFBZUQsU0FBVSxZQUFXLENBQUMsQ0FBQ0UsSUFBSSxDQUNwRUMsSUFBSSxJQUFLO1FBQ1IsTUFBTVAsSUFBSSxHQUFJLE9BQU9PLElBQUksS0FBSyxRQUFRLElBQUlBLElBQUksWUFBWUMsTUFBTSxHQUFJQyxJQUFJLENBQUNDLEtBQUssQ0FBQ0gsSUFBSSxDQUFDLEdBQUdBLElBQUk7UUFDM0YsTUFBTUksT0FBTyxHQUFHLElBQUksQ0FBQ2IsY0FBYyxDQUFDTSxTQUFTLEVBQUVKLElBQUksQ0FBQztRQUVwRCxJQUFJLENBQUNKLFNBQVMsQ0FBQ2dCLEdBQUcsQ0FBQ1IsU0FBUyxFQUFFTyxPQUFPLENBQUM7UUFDdEMsT0FBT0EsT0FBTztNQUNoQixDQUFDLEVBQ0FFLElBQUksSUFBSztRQUNSLElBQUksQ0FBQ0MsUUFBUSxDQUFDRixHQUFHLENBQUNSLFNBQVMsRUFBRSxLQUFLLENBQUM7UUFDbkNXLEtBQUssQ0FBRSxHQUFFRixJQUFJLENBQUNHLE1BQU8sS0FBSUgsSUFBSSxDQUFDSSxJQUFLLEVBQUMsQ0FBQztNQUN2QyxDQUFDLENBQ0Y7SUFDSCxDQUFDO0lBRUQ7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0lBQ0lDLFFBQVEsQ0FBQ0MsRUFBRSxFQUFFO01BQ1gsTUFBTW5CLElBQUksR0FBRyxJQUFJLENBQUNKLFNBQVMsQ0FBQ3dCLEdBQUcsQ0FBQ0QsRUFBRSxDQUFDO01BQ25DLE9BQVFuQixJQUFJLFlBQVlILEdBQUcsSUFBSUcsSUFBSSxLQUFLLEtBQUs7SUFDL0MsQ0FBQztJQUVEO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtJQUNJcUIsU0FBUyxDQUFDRixFQUFFLEVBQUU7TUFDWixNQUFNbkIsSUFBSSxHQUFHLElBQUksQ0FBQ0osU0FBUyxDQUFDd0IsR0FBRyxDQUFDRCxFQUFFLENBQUM7TUFDbkMsT0FBUW5CLElBQUksWUFBWUgsR0FBRyxJQUFJRyxJQUFJLEtBQUssS0FBSyxHQUFJQSxJQUFJLEdBQUcsSUFBSTtJQUM5RCxDQUFDO0lBRUQ7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtJQUNJc0IsUUFBUSxDQUFDSCxFQUFFLEVBQUU7TUFDWCxNQUFNbkIsSUFBSSxHQUFHLElBQUksQ0FBQ0osU0FBUyxDQUFDd0IsR0FBRyxDQUFDRCxFQUFFLENBQUM7TUFFbkMsSUFBSW5CLElBQUksWUFBWUgsR0FBRyxJQUFJRyxJQUFJLEtBQUssS0FBSyxFQUFFO1FBQ3pDLE9BQU91QixPQUFPLENBQUNDLE9BQU8sQ0FBQ3hCLElBQUksQ0FBQztNQUM5QjtNQUVBLElBQUl5QixPQUFPLEdBQUd6QixJQUFJO01BRWxCLElBQUksQ0FBQ0EsSUFBSSxJQUFJLE9BQU9BLElBQUksQ0FBQ00sSUFBSSxLQUFLLFVBQVUsRUFBRTtRQUM1Q21CLE9BQU8sR0FBRyxJQUFJLENBQUN0QixZQUFZLENBQUNnQixFQUFFLENBQUM7UUFDL0IsSUFBSSxDQUFDdkIsU0FBUyxDQUFDZ0IsR0FBRyxDQUFDTyxFQUFFLEVBQUVNLE9BQU8sQ0FBQztNQUNqQztNQUVBLE9BQU9BLE9BQU87SUFDaEI7RUFDRixDQUFDOztFQUVEO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtFQUNFL0IsTUFBTSxDQUFDQyxPQUFPLENBQUNNLFFBQVEsR0FBRztJQUV4QjtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0lBQ0l5QixHQUFHLENBQUMxQixJQUFJLEVBQUU7TUFDUixNQUFNMkIsS0FBSyxHQUFHLElBQUk5QixHQUFHLEVBQUU7TUFFdkJHLElBQUksQ0FBQzRCLE1BQU0sQ0FBQ0MsT0FBTyxDQUFFQyxLQUFLLElBQUs7UUFDN0IsTUFBTUMsUUFBUSxHQUFJRCxLQUFLLENBQUM1QixJQUFJLEtBQUssUUFBUSxHQUNyQ1QsS0FBSyxDQUFDdUMsb0JBQW9CLEdBQzFCdkMsS0FBSyxDQUFDd0MsWUFBWTtRQUV0QkgsS0FBSyxDQUFDSCxLQUFLLENBQUNFLE9BQU8sQ0FBRUssSUFBSSxJQUFLO1VBQzVCUCxLQUFLLENBQUNmLEdBQUcsQ0FBQ3NCLElBQUksQ0FBQ2YsRUFBRSxFQUFFO1lBQ2pCQSxFQUFFLEVBQUVlLElBQUksQ0FBQ2YsRUFBRTtZQUNYZ0IsS0FBSyxFQUFFRCxJQUFJLENBQUNDLEtBQUs7WUFDakJDLElBQUksRUFBRUwsUUFBUSxDQUFDO2NBQUUsR0FBR0csSUFBSTtjQUFFRyxJQUFJLEVBQUVQLEtBQUssQ0FBQ087WUFBSyxDQUFDO1VBQzlDLENBQUMsQ0FBQztRQUNKLENBQUMsQ0FBQztNQUNKLENBQUMsQ0FBQztNQUVGLE9BQU9WLEtBQUs7SUFDZCxDQUFDO0lBRUQ7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7SUFDSVcsU0FBUyxDQUFDdEMsSUFBSSxFQUFFO01BQ2QsTUFBTTJCLEtBQUssR0FBRyxJQUFJOUIsR0FBRyxFQUFFO01BRXZCRyxJQUFJLENBQUM0QixNQUFNLENBQUNDLE9BQU8sQ0FBRUMsS0FBSyxJQUFLO1FBQzdCLE1BQU07VUFBRVM7UUFBSSxDQUFDLEdBQUd2QyxJQUFJO1FBQ3BCLE1BQU13QyxTQUFTLEdBQUd4QyxJQUFJLENBQUN3QyxTQUFTLENBQUNDLE1BQU0sQ0FBQyxDQUFDQyxHQUFHLEVBQUVDLElBQUksS0FBTSxHQUFFRCxHQUFJLElBQUdDLElBQUssRUFBQyxFQUFFLEVBQUUsQ0FBQztRQUU1RWIsS0FBSyxDQUFDSCxLQUFLLENBQUNFLE9BQU8sQ0FBRUssSUFBSSxJQUFLO1VBQzVCUCxLQUFLLENBQUNmLEdBQUcsQ0FBQ3NCLElBQUksQ0FBQ2YsRUFBRSxFQUFFO1lBQ2pCQSxFQUFFLEVBQUVlLElBQUksQ0FBQ2YsRUFBRTtZQUNYZ0IsS0FBSyxFQUFFRCxJQUFJLENBQUNDLEtBQUs7WUFDakJDLElBQUksRUFBRTNDLEtBQUssQ0FBQ21ELGVBQWUsQ0FBQ1YsSUFBSSxFQUFFSyxHQUFHLEVBQUVDLFNBQVM7VUFDbEQsQ0FBQyxDQUFDO1FBQ0osQ0FBQyxDQUFDO01BQ0osQ0FBQyxDQUFDO01BRUYsT0FBT2IsS0FBSztJQUNkLENBQUM7SUFFRDtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtJQUNJa0IsS0FBSyxDQUFDN0MsSUFBSSxFQUFFO01BQ1YsTUFBTTJCLEtBQUssR0FBRyxJQUFJOUIsR0FBRyxFQUFFO01BRXZCRyxJQUFJLENBQUM0QixNQUFNLENBQUNDLE9BQU8sQ0FBRUssSUFBSSxJQUFLO1FBQzVCUCxLQUFLLENBQUNmLEdBQUcsQ0FBQ3NCLElBQUksQ0FBQ2YsRUFBRSxFQUFFO1VBQ2pCQSxFQUFFLEVBQUVlLElBQUksQ0FBQ2YsRUFBRTtVQUNYZ0IsS0FBSyxFQUFFRCxJQUFJLENBQUNDLEtBQUs7VUFDakJDLElBQUksRUFBRTNDLEtBQUssQ0FBQ3dDLFlBQVksQ0FBQ0MsSUFBSTtRQUMvQixDQUFDLENBQUM7TUFDSixDQUFDLENBQUM7TUFFRixPQUFPUCxLQUFLO0lBQ2Q7RUFDRixDQUFDOztFQUVEO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtFQUNFbEMsS0FBSyxDQUFDdUMsb0JBQW9CLEdBQUcsQ0FBQ0UsSUFBSSxFQUFFWSxJQUFJLEdBQUcsQ0FBQyxDQUFDLEtBQUs7SUFDaEQsSUFBSUMsS0FBSyxHQUFJLCtCQUE4QmIsSUFBSSxDQUFDQyxLQUFNLEdBQUU7SUFFeEQsSUFBSUQsSUFBSSxDQUFDYyxPQUFPLEVBQUU7TUFDaEJELEtBQUssSUFBSyxhQUFZYixJQUFJLENBQUNjLE9BQVEsR0FBRTtJQUN2QztJQUVBLElBQUlGLElBQUksQ0FBQ0csTUFBTSxFQUFFO01BQ2ZGLEtBQUssSUFBSyxZQUFXRCxJQUFJLENBQUNHLE1BQU8sR0FBRTtJQUNyQztJQUNBLElBQUlILElBQUksQ0FBQ0ksS0FBSyxFQUFFO01BQ2RILEtBQUssSUFBSyxXQUFVRCxJQUFJLENBQUNJLEtBQU0sR0FBRTtJQUNuQztJQUVBLE9BQVEsUUFBT0gsS0FBTTtBQUN6Qix5QkFBeUJJLFNBQVMsQ0FBQ2pCLElBQUksQ0FBQ0csSUFBSSxDQUFFLElBQUdlLGtCQUFrQixDQUFDbEIsSUFBSSxDQUFDZixFQUFFLENBQUU7QUFDN0UsV0FBVztFQUNULENBQUM7O0VBRUQ7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0VBQ0UxQixLQUFLLENBQUN3QyxZQUFZLEdBQUcsQ0FBQ0MsSUFBSSxFQUFFWSxJQUFJLEdBQUcsQ0FBQyxDQUFDLEtBQUs7SUFDeEMsTUFBTUcsTUFBTSxHQUFHSCxJQUFJLENBQUNHLE1BQU0sSUFBSWYsSUFBSSxDQUFDZSxNQUFNO0lBQ3pDLE1BQU1DLEtBQUssR0FBR0osSUFBSSxDQUFDSSxLQUFLLElBQUloQixJQUFJLENBQUNnQixLQUFLO0lBRXRDLElBQUlILEtBQUssR0FBSSwrQkFBOEJiLElBQUksQ0FBQ0MsS0FBTSxHQUFFO0lBRXhELElBQUljLE1BQU0sRUFBRTtNQUNWRixLQUFLLElBQUssWUFBV0UsTUFBTyxHQUFFO0lBQ2hDO0lBQ0EsSUFBSUMsS0FBSyxFQUFFO01BQ1RILEtBQUssSUFBSyxXQUFVRyxLQUFNLEdBQUU7SUFDOUI7SUFFQSxPQUFRLFFBQU9ILEtBQU0sU0FBUUksU0FBUyxDQUFDakIsSUFBSSxDQUFDRyxJQUFJLENBQUUsS0FBSTtFQUN4RCxDQUFDOztFQUVEO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtFQUNFNUMsS0FBSyxDQUFDbUQsZUFBZSxHQUFHLENBQUNWLElBQUksRUFBRUssR0FBRyxFQUFFQyxTQUFTLEdBQUcsRUFBRSxFQUFFTSxJQUFJLEdBQUcsQ0FBQyxDQUFDLEtBQUs7SUFDaEUsSUFBSUMsS0FBSyxHQUFJLFVBQVUsR0FBRVAsU0FBVSxrQkFBaUIsQ0FBQ2EsSUFBSSxFQUFHLHFCQUFvQm5CLElBQUksQ0FBQ0MsS0FBTSxHQUFFO0lBRTdGLE1BQU1tQixHQUFHLEdBQUdSLElBQUksQ0FBQ0csTUFBTSxJQUFJSCxJQUFJLENBQUNJLEtBQUs7SUFDckMsSUFBSUksR0FBRyxFQUFFO01BQ1BQLEtBQUssSUFBSyxzQkFBcUJPLEdBQUksTUFBSztJQUMxQztJQUVBLE9BQVEsSUFBR2YsR0FBSSxJQUFHUSxLQUFNLElBQUdRLGtCQUFrQixDQUFDckIsSUFBSSxDQUFDc0IsT0FBTyxDQUFFLEtBQUlqQixHQUFJLEdBQUU7RUFDeEUsQ0FBQztBQUNILENBQUMsRUFBRTdDLE1BQU0sQ0FBQyJ9
