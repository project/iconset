<?php

namespace Drupal\iconset\Asset;

/**
 * A file asset that wraps and manages a SVG sprite file with multiple icons.
 */
class SvgSpriteAsset implements FileAssetInterface {

  /**
   * The path to the SVG sprite file.
   *
   * @var string
   */
  protected $filepath;

  /**
   * An array of data for icons in the SVG sprite file. Icons are keyed by ID.
   *
   * @var array
   */
  protected $icons;

  /**
   * Create a new instance of the SvgSpriteAsset class.
   *
   * @param string $filepath
   *   The path of the SVG sprite file.
   * @param array $icons
   *   An array of icon data, which is keyed by the icon ID, and has the label
   *   and viewBox information for each icon managed by this asset.
   */
  public function __construct($filepath, array $icons) {
    $this->filepath = $filepath;
    $this->icons = $icons;
  }

  /**
   * {@inheritdoc}
   */
  public function getFilepath() {
    return $this->filepath;
  }

  /**
   * {@inheritdoc}
   */
  public function getIcons() {
    return $this->icons;
  }

  /**
   * {@inheritdoc}
   */
  public function getIcon($icon_id) {
    return $this->icons[$icon_id] ?? FALSE;
  }

}
