<?php

namespace Drupal\iconset\Asset;

/**
 * Asset file which wraps a SVG icon file.
 */
class ImageAsset implements FileAssetInterface {

  /**
   * The path of the SVG file.
   *
   * @var string
   */
  protected $filepath;

  /**
   * The SVG information (ID, label, and dimensions / viewBox).
   *
   * @var array
   */
  protected $info;

  /**
   * Create a new instance of the SvgIconAsset.
   *
   * @param string $filepath
   *   The path of the icon file.
   * @param array $icon_info
   *   Discovered information about the icon (ID, label and viewBox).
   */
  public function __construct($filepath, array $icon_info) {
    $this->filepath = $filepath;
    $this->info = $icon_info;
  }

  /**
   * {@inheritdoc}
   */
  public function getFilepath() {
    return $this->filepath;
  }

  /**
   * {@inheritdoc}
   */
  public function getIcons() {
    return [
      $this->info['id'] => $this->info,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIcon($icon_id) {
    return $icon_id == $this->info['id'] ? $this->info : FALSE;
  }

}
