<?php

namespace Drupal\iconset\Asset;

/**
 * Interface for icon assets which are files.
 */
interface FileAssetInterface extends AssetInterface {

  /**
   * Get the path of the icon file asset.
   *
   * @return string
   *   The path for the asset file.
   */
  public function getFilepath();

}
