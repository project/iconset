<?php

namespace Drupal\iconset\Asset;

/**
 * Interface for assets which have icons.
 *
 * Assets are defined in the iconset definitions, and are parsed and loaded by
 * the icon plugin handler.
 *
 * The default iconset implementation will try to cache the asset, and
 * implementations should be prepared to have their instances serialized
 * correctly (__sleep(), __wakeup() or implement \Serializable interface).
 */
interface AssetInterface {

  /**
   * Get icon data for all icons this asset contains.
   *
   * @return array
   *   The icon data for all the icons contained by this asset. Information
   *   should always include the icon ID and label. Assets may include
   *   implementation specific data, like "viewBox" attributes for SVGs.
   */
  public function getIcons();

  /**
   * Get the icon information for the icon identified by the $icon_id.
   *
   * @param string $icon_id
   *   The ID of the icon to fetch the data for.
   *
   * @return array|false
   *   Get the icon data for the requested icon identified by $icon_id. If
   *   icon is not available in this asset, then return boolean FALSE.
   */
  public function getIcon($icon_id);

}
