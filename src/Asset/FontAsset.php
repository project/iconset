<?php

namespace Drupal\iconset\Asset;

/**
 * An asset which wraps information about an icon font-face.
 *
 * The asset includes data about the font-face (font family, weight, etc) and
 * the characters in the font which resolve to icons in this font-face.
 */
class FontAsset implements AssetInterface {

  /**
   * The font face identifier for this font base iconset.
   *
   * @var string
   */
  protected $font;

  /**
   * The list of icon data keyed by the icon ID.
   *
   * @var array
   */
  protected $icons;

  /**
   * Creates a new instance of FontAsset, to wrap data about an icon font-face.
   *
   * @param array $font
   *   Information about the font-face if available (family, weight, etc..).
   * @param array $icons
   *   An array of icons keyed by ID, and should contain label and unicode data
   *   need to render the icon font.
   */
  public function __construct(array $font, array $icons = []) {
    $this->font = $font;
    $this->icons = $icons;
  }

  /**
   * {@inheritdoc}
   */
  public function getFontInfo() {
    return $this->font;
  }

  /**
   * {@inheritdoc}
   */
  public function getIcons() {
    return $this->icons;
  }

  /**
   * {@inheritdoc}
   */
  public function getIcon($icon_id) {
    return $this->icons[$icon_id] ?? FALSE;
  }

}
