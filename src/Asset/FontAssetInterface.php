<?php

namespace Drupal\iconset\Asset;

/**
 * Interface for assets which wrap information about an icon font-face.
 *
 * The asset includes data about the font-face (font family, weight, etc) and
 * the characters in the font which resolve to icons in this font-face.
 */
interface FontAssetInterface extends AssetInterface {

  /**
   * Get information about the font-face wrapped by this asset.
   *
   * @return array
   *   The font-family, font-weight, font-stretch and unicode-range if the
   *   information was available. It's possible this array is empty if not
   *   information was included with the asset.
   */
  public function getFontInfo();

}
