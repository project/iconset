<?php

namespace Drupal\iconset\ParamConverter;

use Symfony\Component\Routing\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\ParamConverter\ParamConverterInterface;
use Drupal\iconset\IconsetManagerInterface;

/**
 * URL parameter converter for iconset plugins from plugin ID (string).
 */
class IconsetParamConverter implements ParamConverterInterface {

  use LoggerChannelTrait;

  /**
   * The iconset plugin manager.
   *
   * @var \Drupal\iconset\IconsetManagerInterface
   */
  protected $iconsetManager;

  /**
   * Create a new instance of the IconsetParamConverter class.
   *
   * @param \Drupal\iconset\IconsetManagerInterface $iconset_manager
   *   The iconset plugin manager.
   */
  public function __construct(IconsetManagerInterface $iconset_manager) {
    $this->iconsetManager = $iconset_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function convert($value, $definition, $name, array $defaults) {
    try {
      if (preg_match('/[^\w\-_]/', $value)) {
        throw new \InvalidArgumentException('The URL parameter for iconset has invalid characters: ' . $value);
      }

      return $this->iconsetManager->getInstance($value);
    }
    catch (\Exception $e) {
      $this
        ->getLogger('iconset')
        ->error('Error %type while converting iconset URL parameter with message: %message', [
          '%type' => get_class($e),
          '%message' => $e->getMessage(),
        ]);

      throw new NotFoundHttpException();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function applies($definition, $name, Route $route) {
    return !empty($definition['type']) && $definition['type'] === 'iconset';
  }

}
