<?php

namespace Drupal\iconset\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Plugin annotation object for icon handler plugins.
 *
 * @see \Drupal\Component\Annotation\Plugin
 * @see \Drupal\iconset\Plugin\IconHandlerInterface
 * @see \Drupal\iconset\Plugin\IconHandlerPluginManager
 * @see plugin_api
 *
 * @ingroup iconset_handler_plugins
 *
 * @Annotation
 */
class IconsetIconHandler extends Plugin {

  /**
   * The Icon plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human friendly name of the Sassifrass compiler plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label = '';

  /**
   * Help or description or help text to help understand what this plugin is.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $help = '';

  /**
   * If a file based icon handler, does it support directories as assets.
   *
   * @var bool
   */
  public $allow_directory = FALSE;

  /**
   * The file extension (including the period), of valid files.
   *
   * This annotation value is optional, and not use for most of the icon
   * handler plugins. Some icon handlers which might support different file
   * options might require this (eg \DirectoryHandler).
   *
   * @var string
   */
  public $file_extension;

  /**
   * The list Drupal libraries that should be included for JS handling.
   *
   * @var array
   */
  public $libraries = [];

}
