<?php

namespace Drupal\iconset;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\iconset\Asset\AssetInterface;
use Drupal\iconset\Exception\IconNotFoundException;
use Drupal\toolshed\Strategy\ContainerInjectionStrategyInterface;
use Drupal\toolshed\Strategy\StrategyBase;
use Drupal\toolshed\Strategy\StrategyDefinitionInterface;
use Drupal\toolshed\Strategy\StrategyInterface;

/**
 * The default iconset plugin, manages a sets described by modules and themes.
 *
 * Modules and themes can provide iconsets for use with menu icons, themes and
 * other general purposes. This class is the default implementation for
 * locating, enumerating and rendering icons provided by these sets.
 */
class IconsetDefault extends StrategyBase implements IconsetInterface, ContainerInjectionStrategyInterface {

  /**
   * An array of icon ID's mapped to their respective assets.
   *
   * @var array
   */
  protected $iconMap;

  /**
   * Array of iconset asset instances.
   *
   * @var \Drupal\iconset\Asset\AssetInterface[]
   */
  protected $assets;

  /**
   * The cache backend to store discovered icons from.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * An array of icon handlers being used, keyed by the resource name.
   *
   * @var \Drupal\iconset\Plugin\IconHandlerInterface
   */
  protected $handler;

  /**
   * Icon builder to use in generating renderable array for an icon in this set.
   *
   * @var \Drupal\iconset\IconBuilderInterface
   */
  protected $builder;

  /**
   * Create a new instance of a iconset.
   *
   * @param string $id
   *   The ID of this plugin, or the iconset name.
   * @param \Drupal\toolshed\Strategy\StrategyDefinitionInterface $definition
   *   The iconset definition, which contains the handler type and resources.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $icon_handler_manager
   *   Plugin manager responsible for managing icon handlers.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend to use when caching iconset data.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(string $id, StrategyDefinitionInterface $definition, PluginManagerInterface $icon_handler_manager, CacheBackendInterface $cache_backend) {
    $this->id = $id;
    $this->definition = $definition;

    $pluginId = $definition->plugin ?? NULL;
    if (!$pluginId) {
      $msg = sprintf('Iconset definition "%s" is missing an icon handler plugin.', $id);
      throw new \InvalidArgumentException($msg);
    }

    $this->handler = $icon_handler_manager->createInstance(
      $pluginId,
      $definition->config ?? []
    );

    $this->builder = $this->handler;
    $this->cache = $cache_backend;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, string $id, StrategyDefinitionInterface $definition): StrategyInterface {
    return new static(
      $id,
      $definition,
      $container->get('plugin.manager.iconset.icon_handler'),
      $container->get('cache.iconset')
    );
  }

  /**
   * Internal method for loading icons if they have not been loaded yet.
   *
   * Method is responsible for managing icon data cache and creating the plugin
   * assets into a usable format.
   *
   * @see \Drupal\iconset\Plugin\IconsetHandlerInterface::createAsset()
   */
  protected function loadIcons() {
    if ($this->getProviderType() === 'module') {
      $cid = $this->id();
    }
    else {
      $cid = implode(':', [
        $this->getProviderType(),
        $this->getProvider(),
        $this->id(),
      ]);
    }

    if ($this->cache && $cached = $this->cache->get($cid)) {
      $this->assets = $cached->data['assets'] ?? [];
      $this->iconMap = $cached->data['iconMap'];
    }
    else {
      $key = 0;
      $this->assets = [];
      $this->iconMap = [];

      foreach ($this->getDefinition()->assets ?? [] as $assetUri) {
        if ($asset = $this->handler->createAssets($assetUri, $this)) {
          $assets = $asset instanceof AssetInterface ? [$asset] : $asset;

          foreach ($assets as $item) {
            if ($icons = $item->getIcons()) {
              $this->assets[$key] = $item;
              $this->iconMap += array_fill_keys(array_keys($icons), $key);
              ++$key;
            }
          }
        }
      }

      // Cache the built assets and icon to asset mapping data.
      $this->cache->set($cid, [
        'assets' => $this->assets,
        'iconMap' => $this->iconMap,
      ]);
    }
  }

  /**
   * Find the icon asset which owns the icon specified by the $icon_id.
   *
   * @param string $icon_id
   *   The ID of the icon to find. Method will return the asset which manages
   *   the icon matching this ID.
   *
   * @return \Drupal\iconset\Asset\AssetInterface
   *   The asset from this iconset which owns the icon requested in $icon_id.
   *
   * @throws \Drupal\iconset\Exception\IconNotFoundException
   *   Is thrown when the icon cannot be found, or method is unable to determine
   *   which asset manages this icon.
   */
  protected function getAsset($icon_id) {
    if (!isset($this->assets)) {
      $this->loadIcons();
    }

    $key = $this->iconMap[$icon_id] ?? NULL;
    if (isset($key) && isset($this->assets[$key])) {
      $key = $this->iconMap[$icon_id];
      return $this->assets[$key];
    }

    throw new IconNotFoundException($this->id(), $icon_id);
  }

  /**
   * {@inheritdoc}
   */
  public function setBuilder(IconBuilderInterface $builder) {
    $this->builder = $builder;
  }

  /**
   * {@inheritdoc}
   */
  public function getIcons() {
    if (!isset($this->assets)) {
      $this->loadIcons();
    }

    $icons = [];
    foreach ($this->assets as $asset) {
      $icons += $asset->getIcons();
    }

    return $icons;
  }

  /**
   * {@inheritdoc}
   */
  public function getIcon($icon_id) {
    $asset = $this->getAsset($icon_id);
    return $asset->getIcon($icon_id);
  }

  /**
   * {@inheritdoc}
   */
  public function fetchJson() {
    if (!isset($this->assets)) {
      $this->loadIcons();
    }

    $json = $this->handler->getJsSettings();
    $json['assets'] = [];

    foreach ($this->assets as $asset) {
      $json['assets'][] = $this->handler->formatJson($asset);
    }

    return $json;
  }

  /**
   * {@inheritdoc}
   */
  public function build($icon_id, array $options = []) {
    $asset = $this->getAsset($icon_id);
    return $this->builder->build($icon_id, $asset, $options);
  }

}
