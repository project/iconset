<?php

namespace Drupal\iconset\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Configure how iconsets are loaded and icons are used.
 */
class IconsetSettingsForm extends ConfigFormBase {

  /**
   * Drupal cache fetch / create service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $iconsetCache;

  /**
   * Create a new instance of a configuration form for managing JS breakpoints.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\Cache\CacheBackendInterface $iconset_cache
   *   The cache backend for iconset plugin data.
   */
  public function __construct(ConfigFactoryInterface $config_factory, CacheBackendInterface $iconset_cache) {
    parent::__construct($config_factory);

    $this->iconsetCache = $iconset_cache;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('cache.iconset')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'iconset_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return ['iconset.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['clear_icon_cache'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Clear Icon Cache'),
      '#description' => $this->t('Use this when updating icon files and new icons need to be discovered.'),

      'actions' => [
        '#type' => 'actions',

        'clear_cache' => [
          '#type' => 'submit',
          '#value' => $this->t("Clear Cache"),
          '#submit' => [
            [$this, 'clearIconCacheSubmit'],
          ],
        ],
      ],
    ];

    return $form;
  }

  /**
   * Form submit callback, which performs the iconset cache flush action.
   *
   * @param array $form
   *   The form elements and structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state, values and build information.
   */
  public function clearIconCacheSubmit(array &$form, FormStateInterface $form_state) {
    // Ensure that all the icon discovery caches have been cleared.
    $this->iconsetCache->deleteAll();
    Cache::invalidateTags(['iconset.list']);

    $this
      ->messenger()
      ->addMessage($this->t('Icon discovery caches has been cleared.'));
  }

}
