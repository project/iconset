<?php

namespace Drupal\iconset\Exception;

use Drupal\iconset\IconsetInterface;

/**
 * Exception to throw when a requested icon is not available.
 *
 * @todo Apply this exception in to the IconsetPluginInterface::build()
 * methods and other places where icons are requested when able to update
 * the callers to properly handle the exception.
 */
class IconNotFoundException extends \Exception {

  /**
   * Create a new instance of the IconNotFoundException.
   *
   * @param string|\Drupal\iconset\IconsetInterface $iconset
   *   Machine name or plugin instance of the iconset of the icon being
   *   requested.
   * @param string $icon
   *   The identifier for the icon being requested.
   * @param int $code
   *   The error code (unused).
   * @param \Exception $previous
   *   A previous exception, when exceptions are being chained or nested.
   */
  public function __construct($iconset, $icon, $code = 0, \Exception $previous = NULL) {
    $iconset = $iconset instanceof IconsetInterface ? $iconset->id() : $iconset;
    $msg = sprintf('Unable to find icon of %s in %s.', $icon, $iconset);

    parent::__construct($msg, $code, $previous);
  }

}
