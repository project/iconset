<?php

namespace Drupal\iconset\Twig;

use Drupal\Core\Template\Attribute;
use Twig\TwigFunction;
use Twig\Extension\AbstractExtension;

/**
 * A class providing easy rendering of iconset icons to Twig templates.
 */
class IconsetTwigExtension extends AbstractExtension {

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return 'iconset';
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new TwigFunction('iconset_icon', [$this, 'buildIcon']),
    ];
  }

  /**
   * Builder a renderable array of the specified Iconset icon.
   *
   * @param string $iconset
   *   Iconset ID for the set the requested icon is from.
   * @param string $icon_id
   *   Machine name identifier for the icon to render.
   * @param array|\Drupal\Core\Template\Attribute $attributes
   *   An optional array or Attribute object of link attributes.
   *
   * @return array
   *   A render array representing an Iconset icon.
   */
  public function buildIcon($iconset, $icon_id, $attributes = []) {
    assert(is_array($attributes) || $attributes instanceof Attribute, '$attributes, if set, must be an array or object of type \Drupal\Core\Template\Attribute');

    $build = [
      '#type' => 'iconset_icon',
      '#iconset' => $iconset,
      '#icon' => $icon_id,
    ];

    if ($attributes instanceof Attribute) {
      $attributes = $attributes->toArray();
    }

    // Move any width and height information into the render array so the
    // iconset handler will handle it in the appropriate icon type way.
    if (!empty($attributes['width'])) {
      $build['#width'] = $attributes['width'];
      unset($attributes['width']);
    }
    if (!empty($attributes['height'])) {
      $build['#height'] = $attributes['height'];
      unset($attributes['height']);
    }

    // Apply any remaining attributes to the icon rendering.
    $build['#attributes'] = $attributes;
    return $build;
  }

}
