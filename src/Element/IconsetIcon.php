<?php

namespace Drupal\iconset\Element;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Render\Element\RenderElement;
use Drupal\iconset\Exception\IconNotFoundException;
use Drupal\iconset\IconsetManagerInterface;

/**
 * Render a single icon from an iconset.
 *
 * Properties:
 * - #iconset: The iconset identifier for the set the icon belongs to.
 * - #icon: The ID of the icon to render.
 * - #alt_text: Alt text to use for accessibility or if browser fails to render.
 *
 * Usage example:
 * @code
 * $build['example_icon'] = [
 *   '#type' => 'iconset_icon',
 *   '#iconset' => 'fontawesome_brands',
 *   '#icon' => 'facebook_square',
 *   '#widith' => 40,
 *   '#height' => 40,
 *   '#alt_text => t('Facebook'),
 *   '#attributes => [
 *     'class' => ['icon--large'],
 *   ],
 * ];
 * @endcode
 *
 * @RenderElement("iconset_icon")
 */
class IconsetIcon extends RenderElement {

  /**
   * The iconset manager.
   *
   * @var \Drupal\iconset\IconsetManagerInterface
   */
  protected static $iconsetManager;

  /**
   * Fetch the iconset manager service for getting iconset instances.
   *
   * @return \Drupal\iconset\IconsetManagerInterface
   *   Get the iconset manager.
   */
  protected static function getIconsetManager(): IconsetManagerInterface {
    if (!isset(self::$iconsetManager)) {
      self::$iconsetManager = \Drupal::service('strategy.manager.iconset');
    }

    return self::$iconsetManager;
  }

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return [
      '#iconset' => '',
      '#icon' => '',
      '#alt_text' => '',
      '#attributes' => [],
      '#decorative' => FALSE,
      '#pre_render' => [static::class . '::preRender'],
    ];
  }

  /**
   * Transform the icon information into a renderably array of theme elements.
   *
   * @param array $element
   *   The element to alter the rendering content for.
   *
   * @return array
   *   Renderable array to render the icon.
   */
  public static function preRender(array $element) {
    if (!(empty($element['#iconset']) || empty($element['#icon']))) {
      try {
        $iconsetManager = static::getIconsetManager();
        $options = [];
        $options['decorative'] = $element['#decorative'];

        if ($element['#alt_text']) {
          $options['alt'] = trim($element['#alt_text']);
        }

        // Though most icon types (images) support width and height as
        // HTML tag attributes, we push this information as options, and leave
        // it to the iconset builder to determine how to use this information.
        //
        // This allows more flexibility with implementations.
        if (!empty($element['#width'])) {
          $options['width'] = intval($element['#width']);
        }
        if (!empty($element['#height'])) {
          $options['height'] = intval($element['#height']);
        }

        /** @var \Drupal\iconset\IconsetInterface $iconset */
        $iconset = $iconsetManager->getInstance($element['#iconset']);
        $content = $iconset->build($element['#icon'], $options);

        if ($content) {
          if (!empty($content['#attributes']['class'])) {
            $element['#attributes'] += ['class' => []];
            $element['#attributes']['class'] = array_merge($content['#attributes']['class'], $element['#attributes']['class']);
          }
          $content['#attributes'] = $element['#attributes'] + $content['#attributes'];

          return $content;
        }
      }
      catch (PluginException | IconNotFoundException $e) {
        // @todo Render a default, or no image icon placeholder as a fallback.
      }
    }

    return [];
  }

}
