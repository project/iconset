<?php

namespace Drupal\iconset\Element;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Render\Element\FormElement;
use Drupal\Core\Form\FormStateInterface;
use Drupal\iconset\Exception\IconNotFoundException;

/**
 * Form element for creating an icon selector form element.
 *
 * Properties:
 * - #iconset: The iconsets to allow for selection.
 * - #icon: The ID of the icon to render.
 * - #alt_text: Alt text to use for accessibility or if browser fails to render.
 *
 * Usage example:
 * @code
 * $build['example_icon_selector'] = [
 *   '#type' => 'iconset_selector',
 *   '#iconset' => 'fontawesome_brands',
 *   '#default_value' => [
 *      'iconset' => 'fontawesome_brands',
 *      'icon' => 'facebook',
 *   ],
 * ];
 * @endcode
 *
 * @FormElement("iconset_selector")
 */
class IconsetSelector extends FormElement {

  /**
   * The default value format with an array with "iconset" and "icon" keys.
   */
  const ARRAY_FORMAT = 'array';

  /**
   * Compact format stores and expects the iconset icon value as a string.
   *
   * Compact format is only available for single select values. Any multi-select
   * icon selectors will always expect and store values in the array format.
   *
   * The format expects the string to be "[iconset]:[icon]".
   */
  const COMPACT_FORMAT = 'compact';

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return [
      '#input' => TRUE,
      '#multiple' => FALSE,
      '#required' => FALSE,
      '#format' => self::ARRAY_FORMAT,
      '#iconset' => [],
      '#process' => [
        static::class . '::processIconset',
        static::class . '::processAjaxForm',
      ],
      '#element_validate' => [
        static::class . '::validateIconSelect',
      ],
      '#pre_render' => [],
      '#theme_wrappers' => ['iconset_selector_wrapper'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    if ($element['#multiple'] && is_string($input['icon'])) {
      return [
        'iconset' => $input['iconset'],
        'icon' => preg_split('#\s*,\s*#', $input['icon'], -1, PREG_SPLIT_NO_EMPTY),
      ];
    }
  }

  /**
   * Processes a select list form element.
   *
   * This process callback is mandatory for select fields, since all user agents
   * automatically preselect the first available option of single (non-multiple)
   * select lists.
   *
   * @param array $element
   *   The form element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed element.
   */
  public static function processIconset(array &$element, FormStateInterface $form_state, array &$complete_form) {
    $iconsetManager = \Drupal::service('strategy.manager.iconset');
    $iconHandlerManager = \Drupal::service('plugin.manager.iconset.icon_handler');

    // Ensure that all the required icon handler JS libraries are included.
    $jsLibs = ['iconset/icon-selector' => 'iconset/icon-selector'];
    foreach ($iconHandlerManager->getDefinitions() as $def) {
      $jsLibs += array_combine($def['libraries'], $def['libraries']);
    }

    $value = [];
    if (!empty($element['#value'])) {
      if (is_string($element['#value'])) {
        $parts = explode(':', $element['#value'], 2);
        $value = [
          'iconset' => $parts[0],
          'icon' => $parts[1] ?? '',
        ];
      }
      else {
        $value = $element['#value'];
      }
    }

    // Apply value defaults to make sure value array isn't empty.
    $value += [
      'iconset' => '',
      'icon' => $element['#multiple'] ? [] : '',
    ];

    $element['#tree'] = TRUE;
    $element['#attached']['library'] = array_values($jsLibs);

    // Determine which iconsets are available to be selected, and what the
    // current default iconset currently is.
    if (!is_array($element['#iconset']) || count($element['#iconset']) === 1) {
      $iconset = is_array($element['#iconset']) ? reset($element['#iconset']) : $element['#iconset'];
      $element['iconset'] = [
        '#type' => 'hidden',
        '#value' => $iconset,
        '#parents' => array_merge($element['#parents'], ['iconset']),
      ];
    }
    else {
      $iconset = NULL;
      $iconsetDefs = $iconsetManager->getDefinitions();

      $iconsets = [];
      if (!empty($element['#iconset'])) {
        foreach ($element['#iconset'] as $iconset_id) {
          if (isset($iconsetDefs[$iconset_id])) {
            $iconsets[$iconset_id] = $iconsetDefs[$iconset_id]->label ?? $iconset_id;
          }
        }
      }
      else {
        foreach ($iconsetDefs as $iconset_id => $iconsetInfo) {
          $iconsets[$iconset_id] = $iconsetInfo->label ?? $iconset_id;
        }
      }

      if (!empty($value['iconset']) && isset($iconsets[$value['iconset']])) {
        $iconset = $value['iconset'];
      }

      $element['iconset'] = [
        '#type' => 'select',
        '#title' => t('Iconset'),
        '#title_display' => 'invisible',
        '#parents' => array_merge($element['#parents'], ['iconset']),
        '#options' => $iconsets,
        '#default_value' => $iconset,
        '#chosen' => FALSE,
        '#multiple' => FALSE,
      ];

      if ($element['#required']) {
        $element['#required'] = TRUE;
      }
      else {
        $element['#empty_option'] = t('- Select -');
      }
    }

    // Selection of the icon will be different depending on if
    // multiple select is allowed or not.
    $element['icon'] = [
      '#title' => t('Select icon'),
      '#title_display' => 'invisible',
      '#required' => $element['#required'],
    ];

    if ($element['#multiple']) {
      $element['icon']['#type'] = 'textarea';
      $element['icon']['#default_value'] = implode(',', $value['icon']);
      $element['#attributes']['class'][] = 'iconset-selector--multi';
    }
    else {
      $element['icon']['#type'] = 'textfield';
      $element['icon']['#default_value'] = $value['icon'];
    }

    $element['iconset']['#attributes']['class'][] = 'icon-sel__iconset';
    $element['icon']['#attributes']['class'][] = 'icon-sel__icon';

    return $element;
  }

  /**
   * Ensure that the icon selected is available in the selected iconset.
   *
   * Uses a open ended textfield instead of a select element. This avoids the
   * need for AJAX form rebuilds as the iconset is change and the form element
   * options change. This element validate check is to ensure that the final
   * icon selected is a valid iconset and icon combination.
   *
   * @param array $element
   *   Definition of the icon selector form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form build and state.
   * @param array $form
   *   The complete form structure.
   */
  public static function validateIconSelect(array $element, FormStateInterface $form_state, array $form) {
    $icon = $form_state->getValue($element['#parents']);

    if (!empty($icon['iconset']) && !empty($icon['icon'])) {
      $iconsetManager = \Drupal::service('strategy.manager.iconset');

      try {
        /** @var \Drupal\iconset\IconsetInterface $iconset */
        $iconset = $iconsetManager->getInstance($icon['iconset']);
        $iconset->getIcon($icon['icon']);

        // Convert to compact string format if not multiple select, and the
        // compact format option is selected.
        if (empty($element['#multiple']) && $element['#format'] === self::COMPACT_FORMAT) {
          $form_state->setValue($element['#parents'], $icon['iconset'] . ':' . $icon['icon']);
        }
      }
      catch (PluginException | IconNotFoundException $e) {
        // Either the iconset or the icon are not available.
        $form_state->setError($element, t('Invalid icon selected, unable to find the %icon from the %iconset iconset.', [
          '%iconset' => $icon['iconset'],
          '%icon' => $icon['icon'],
        ]));
      }
    }
  }

}
