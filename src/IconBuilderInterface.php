<?php

namespace Drupal\iconset;

use Drupal\iconset\Asset\AssetInterface;

/**
 * Interface for classes use to build icon data into renderable arrays.
 *
 * Interface for classes which can be used to convert icon data into renderable
 * arrays. The icon handler will implement this and generally be the default
 * builder for iconsets.
 */
interface IconBuilderInterface {

  /**
   * Retrieves a renderable version of the requested icon.
   *
   * Retrieves a renderable version of the requested icon. When the icon
   * is not available, this method will return either an empty array or a
   * default 'icon not found' icon as defined by the Toolshed icons module
   * configurations.
   *
   * @param string $icon_id
   *   ID of the icon in this icon set to render.
   * @param \Drupal\iconset\Asset\AssetInterface $iconset
   *   The icon asset resource that has the requested icon.
   * @param array $options
   *   Optional icon rendering parameters which can include the preferred width
   *   and height of the icon, and alt text.
   *
   * @return array
   *   Drupal renderable array representing the icon requested.
   */
  public function build($icon_id, AssetInterface $iconset, array $options = []);

}
