<?php

namespace Drupal\iconset;

use Drupal\toolshed\Strategy\StrategyInterface;

/**
 * Interface for implementing an iconset.
 *
 * Iconsets are a plugin representing a collection of icons which are available
 * to be Drupal rendering, fields, menus and filter text formats.
 */
interface IconsetInterface extends StrategyInterface {

  /**
   * Set the builder object which builds icons into renderables for this set.
   *
   * @param \Drupal\iconset\IconBuilderInterface $builder
   *   Builder object to use to build renderable icons for this set.
   */
  public function setBuilder(IconBuilderInterface $builder);

  /**
   * Get a list of all the icons available for this iconset.
   *
   * The icons provided as an associative array with the keys and values equal
   * to the icon ID and icon information respectively. The icon information
   * lists a reference to the icon handler, viewbox and a label.
   *
   * @return array
   *   Gets a built list of icons that are in this iconset. Array is keyed by
   *   the icon ID and the array values are the icon information for each of
   *   the icons listed.
   *
   * @see Drupal\iconset\IconsetPluginInterface::build()
   */
  public function getIcons();

  /**
   * Extract the icon data into a consumable JSON for frontend Javascript.
   *
   * @return array
   *   Data about the iconset organized by the handler that supports them,
   *   and the data specific for that handler to support creation of the icon
   *   displays for a frontend interface, like Javascript. The data should be
   *   presented as numerically indexed arrays of handler data.
   *
   * @see Drupal\iconset\Plugin\IconHandlerInterface::formatJson()
   */
  public function fetchJson();

  /**
   * Get information about a specific icon.
   *
   * @param string $icon_id
   *   The ID of the icon to retrieve information of.
   *
   * @return array
   *   Information about the icon includes the ID, label, viewbox and other
   *   icon handler specific data.
   *
   * @throws \Drupal\iconset\Exception\IconNotFoundException
   *   Exception for when $icon_id does not exist in this iconset.
   */
  public function getIcon($icon_id);

  /**
   * Build the requested icon in a renderable form.
   *
   * @param string $icon_id
   *   The ID of the icon to convert to a renderable array.
   * @param array $options
   *   Icon rendering options such as ALT text, width and height.
   *
   * @return array
   *   Returns a renderable array of the icon requested.
   *
   * @throws \Drupal\iconset\Exception\IconNotFoundException
   *   Exception for when $icon_id does not exist in this iconset.
   */
  public function build($icon_id, array $options = []);

}
