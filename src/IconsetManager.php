<?php

namespace Drupal\iconset;

use Drupal\Component\Discovery\DiscoverableInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Utility\Error;
use Drupal\toolshed\Discovery\YamlDiscovery;
use Drupal\toolshed\Event\StrategyDefinitionAlterEvent;
use Drupal\toolshed\Strategy\StrategyInterface;
use Drupal\toolshed\Strategy\StrategyManager;

/**
 * Plugin manager for finding and creating iconsets instances.
 *
 * @todo Convert the IconsetManager to use the ThemeAwareStrategyManager, when
 * ready to handle the icon switching for themes.
 */
class IconsetManager extends StrategyManager implements IconsetManagerInterface {

  use LoggerChannelTrait;

  /**
   * {@inheritdoc}
   */
  protected string $strategyInterface = IconsetInterface::class;

  /**
   * List of internal and global iconset labels, keyed by the iconset ID.
   *
   * @var string[]|null
   */
  protected ?array $iconsetLabels;

  /**
   * The theme handler service for Drupal.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected ?ThemeHandlerInterface $themeHandler;

  /**
   * Dependency injection class resolver.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected ClassResolverInterface $classResolver;

  /**
   * Create new instance of the Iconset plugin manager.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The service class that manages the modules and invokes module hooks.
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $theme_handler
   *   The theme handler.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The caching backend to use for caching plugin definitions.
   * @param \Drupal\Core\DependencyInjection\ClassResolverInterface $class_resolver
   *   Dependency injection class resolver.
   */
  public function __construct(ModuleHandlerInterface $module_handler, ThemeHandlerInterface $theme_handler, CacheBackendInterface $cache_backend, ClassResolverInterface $class_resolver) {
    parent::__construct(
      'iconset',
      $module_handler,
      $cache_backend,
      ['iconset']
    );

    $this->strategyInterface = IconsetInterface::class;
    $this->themeHandler = $theme_handler;
    $this->classResolver = $class_resolver;
  }

  /**
   * {@inheritdoc}
   */
  public function clearCachedDefinitions(): void {
    parent::clearCachedDefinitions();
    $this->iconsetLabels = NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultStrategyClass(array $definition): string {
    return IconsetDefault::class;
  }

  /**
   * Get discovery handler for theme provided iconsets.
   *
   * @return \Drupal\Component\Discovery\DiscoverableInterface
   *   Discovery handler for finding all the theme provided iconset definitions.
   */
  protected function getThemeDiscovery(): DiscoverableInterface {
    $dirs = $this->themeHandler->getThemeDirectories();

    $discovery = new YamlDiscovery($this->getDiscoveryName(), $dirs);
    $discovery->addTranslatableProperty('label', 'label_context');
    $discovery->addTranslatableProperty('description', 'description_context');
    return $discovery;
  }

  /**
   * {@inheritdoc}
   *
   * Add theme defined iconset definitions. For now these are treated just like
   * module defined definitions. In version 3.x and beyond iconset will use
   * the Toolshed ThemeAwareStrategyManager to support theme switching and
   * icon overriding. This version is more for straight D10 compatibility.
   */
  protected function findDefinitions(): array {
    $themeDefs = $this->getThemeDiscovery()->findAll();

    foreach ($themeDefs as $theme => &$definitions) {
      foreach ($definitions as $id => &$definition) {
        $definition += [
          'id' => $id,
          'provider' => $theme,
          'provider_type' => 'theme',
        ];
      }
    }
    unset($definitions, $definition);

    if (!empty($this->alterEventName) && !empty($this->eventDispatcher)) {
      $event = new StrategyDefinitionAlterEvent('theme', $themeDefs);
      $this->eventDispatcher->dispatch($event, $this->alterEventName);
    }

    $strategies = parent::findDefinitions();
    foreach ($themeDefs as $definitions) {
      foreach ($definitions as $id => $def) {
        $strategies[$id] = $this->processDefinition($id, $def);
      }
    }

    return $strategies;
  }

  /**
   * {@inheritdoc}
   */
  public function getIconsets($include_internal = FALSE) {
    if (!isset($this->iconsetLabels)) {
      $this->iconsetLabels = [];

      foreach ($this->getDefinitions() as $id => $definition) {
        $isInternal = $definition->internal ?? FALSE;
        $bucket = $isInternal ? 'internal' : 'global';
        $this->iconsetLabels[$bucket][$id] = $definition->label ?? '';
      }
    }

    $iconsets = $this->iconsetLabels['global'] ?? [];
    if ($include_internal) {
      $iconsets += $this->iconsetLabels['internal'] ?? [];
    }

    return $iconsets;
  }

  /**
   * {@inheritdoc}
   *
   * @return \Drupal\iconset\IconsetInterface
   *   Returns the iconset instance.
   *
   * @phpcs:disable Generic.CodeAnalysis.UselessOverridingMethod.Found
   */
  public function getInstance(string $id, array $contexts = []): IconsetInterface {
    return parent::getInstance($id, $contexts);
  }

  /**
   * {@inheritdoc}
   */
  protected function initInstance(string $id, StrategyInterface $iconset): void {
    if (!$iconset instanceof $this->strategyInterface) {
      $msg = sprintf('Iconsets need to implement %s.', $this->strategyInterface);
      throw new \InvalidArgumentException($msg);
    }

    /** @var \Drupal\iconset\IconsetInterface $iconset */
    $definition = $iconset->getDefinition();

    // Instantiate a builder handler for the iconset if one is specified.
    $builder = $definition->builder;
    if (!empty($builder)) {
      try {
        $builder = $this->classResolver->getInstanceFromDefinition($definition->builder);
        $iconset->setBuilder($builder);
      }
      catch (\Exception $e) {
        // If class resolver throws an Exception it means the builder class is
        // not available or cannot be created. Log the error, but it is "safe"
        // to continue using the default builder.
        $this->getLogger('iconset')
          ->error('Unable to set Iconset builder %type due to @message: @backtrace_string', Error::decodeException($e));
      }
    }
  }

}
