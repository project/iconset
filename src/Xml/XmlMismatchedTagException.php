<?php

namespace Drupal\iconset\Xml;

/**
 * An exception that flags that a XML document has mismatched tags.
 */
class XmlMismatchedTagException extends \Exception {

  /**
   * The XML tag that was encountered that caused the exception.
   *
   * @var string
   */
  public $tag;

  /**
   * The tag that the XML parser expected.
   *
   * @var string
   */
  public $expected;

  /**
   * A list of XML tag names of parent elements to the mismatched tag.
   *
   * @var array
   */
  public $parents;

  /**
   * Create a new exception that reports that an XML tag mismatch appeared.
   *
   * @param string $tag
   *   The tag which was encountered when the mismatch error occurred.
   * @param string $expected
   *   The tag name that was expected.
   * @param array $parents
   *   A list of XML tag names of parent elements to the mismatched tag.
   * @param int $code
   *   The exception code for this exception.
   * @param \Throwable $previous
   *   The previous throwable object that was thrown before this one if as a
   *   chain of thrown exceptions.
   */
  public function __construct($tag, $expected, array $parents = [], int $code = 0, \Throwable $previous = NULL) {
    $message = sprintf(
      'XML mismatch found, expected %s but found %s under [%s]',
      $tag,
      $expected,
      implode(', ', $parents),
    );

    $this->tag = $tag;
    $this->expected = $expected;
    $this->parents = $parents;

    parent::__construct($message, $code, $previous);
  }

}
