<?php

namespace Drupal\iconset;

use Drupal\toolshed\Strategy\StrategyManagerInterface;

/**
 * Plugin manager for finding and creating iconsets instances.
 */
interface IconsetManagerInterface extends StrategyManagerInterface {

  /**
   * Get the labels and Plugin IDs of available iconsets.
   *
   * @param bool $include_internal
   *   Indicates if the results should include iconsets that are marked as being
   *   for internal module or theme usage. This flag helps to limit the list
   *   of icons available on administrative forms.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup[]|string[]
   *   An array keyed by the iconset identifier, and has a value which is the
   *   plugin label.
   */
  public function getIconsets($include_internal = FALSE);

}
