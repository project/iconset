<?php

namespace Drupal\iconset\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\iconset\IconsetInterface;
use Drupal\iconset\IconBuilderInterface;
use Drupal\iconset\Asset\AssetInterface;

/**
 * The base interface for creating icon handler plugins.
 *
 * Icon handlers are plugins which are responsible for creating appropriate
 * types of assets, enumerating icons from the assets, providing information
 * about icons (in PHP or JSON - frontend consumption) and building the render
 * arrays for icons.
 */
interface IconHandlerInterface extends PluginInspectionInterface, IconBuilderInterface {

  /**
   * Generate the appropriate asset instances for icon assets.
   *
   * @param string|array $asset_info
   *   Information from the iconset definition about the asset to generate with
   *   this icon handler plugin.
   * @param \Drupal\iconset\IconsetInterface $iconset
   *   The iconset plugin that this asset is being generated for.
   *
   * @return \Drupal\iconset\Asset\AssetInterface|\Drupal\iconset\Asset\AssetInterface[]|null
   *   If asset can be used and has icons (successful icon discovery), then an
   *   asset instance will be returned. NULL will be returned if asset has no
   *   icons or if icon plugin is not able to load and discover icons.
   */
  public function createAssets($asset_info, IconsetInterface $iconset);

  /**
   * Get the JS key of "Drupal.Iconset.handler" in the "iconset/icon" library.
   *
   * This value points to the correct Javascript method to use when reading
   * the JSON formatted icon data.
   *
   * @return array
   *   The name of "Drupal.Iconset.handler" for reading the JSON icon data.
   */
  public function getJsSettings();

  /**
   * Fetch the data to be consumed by a JS icon handler on the clientside.
   *
   * @param \Drupal\iconset\Asset\AssetInterface $asset
   *   An asset containing icons to format JSON data from.
   *
   * @return array
   *   The handler data formatted to be encoded into JSON in order to handle
   *   the parsing and building of icon data on the browser side. The minimal
   *   information is a `type` and `icons` keys, which tells the Javascript
   *   which icon handler should be used to process the icon data. The data in
   *   the `icons` key, should be keyed by icon ID, so the iconset handler can
   *   reduce duplication.
   */
  public function formatJson(AssetInterface $asset);

}
