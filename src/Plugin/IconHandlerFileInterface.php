<?php

namespace Drupal\iconset\Plugin;

use Drupal\iconset\IconsetInterface;

/**
 * The base interface for creating icon handler plugins which work with files.
 */
interface IconHandlerFileInterface extends IconHandlerInterface {

  /**
   * Get an array of file extensions supported by this icon handler.
   *
   * @return string[]
   *   Get a list of file extensions supported by this icon handler.
   */
  public function getFileExtensions();

  /**
   * Create icon assets based from a single file.
   *
   * @param string $filepath
   *   The path of the file to extract the icons from.
   * @param string|array $asset_info
   *   Information from the iconset definition about the asset to generate with
   *   this icon handler plugin.
   * @param \Drupal\iconset\IconsetInterface $iconset
   *   The iconset the assets are being created for.
   *
   * @return \Drupal\iconset\Asset\AssetInterface|\Drupal\iconset\Asset\AssetInterface[]|null
   *   If asset can be used and has icons (successful icon discovery), then an
   *   asset instance will be returned. NULL will be returned if asset has no
   *   icons or if icon plugin is not able to load and discover icons.
   */
  public function createFileAssets($filepath, $asset_info, IconsetInterface $iconset);

}
