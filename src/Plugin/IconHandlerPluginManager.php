<?php

namespace Drupal\iconset\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * The plugin manager for managing icon handler plugins.
 */
class IconHandlerPluginManager extends DefaultPluginManager {

  /**
   * Creates a new instance of the default icons plugin manager.
   *
   * @param \Traversable $namespaces
   *   The handler for traversing namespaces when discovering
   *   Toolshed icon plugin availability.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The caching backend to use in storing configurations and definitions
   *   of icon plugins.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   Module handler for determining active modules for discover and applying
   *   Drupal alter hooks.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/Iconset',
      $namespaces,
      $module_handler,
      'Drupal\iconset\Plugin\IconHandlerInterface',
      'Drupal\iconset\Annotation\IconsetIconHandler'
    );

    $this->alterInfo('iconset_handler_info');
    $this->setCacheBackend($cache_backend, 'iconset_handler_info');
  }

}
