<?php

namespace Drupal\iconset\Plugin\Iconset;

use Drupal\Component\Utility\Html;
use Drupal\Core\Url;
use Drupal\iconset\IconsetInterface;
use Drupal\iconset\Asset\AssetInterface;
use Drupal\iconset\Asset\ImageAsset;
use Drupal\iconset\Asset\SvgSpriteAsset;
use Drupal\iconset\Plugin\IconHandlerFileBase;
use Drupal\iconset\Xml\XmlMismatchedTagException;

/**
 * Icon plugin for managing SVG icons and sprite files.
 *
 * Icon plugin handler for reading and determining the contents of an SVG file.
 * SVG files can just be an icon or contain a collection of symbols (sprites).
 *
 * A file with sprites either has "symbol" elements, which each represent icons,
 * or can be "g" elements (groups). Group elements are only considered an icon
 * if they are found in a "defs" element.
 *
 * It is recommend to use SVG sprite files, and to use "symbol" elements for
 * each icon in the sprite. Not only is it more efficient, the icons are also
 * easier to theme, because you can apply CSS properties like fill to them.
 *
 * @IconsetIconHandler(
 *   id = "svg",
 *   label = @Translation("SVG icons and sprites"),
 *   help = @Translation("Enumerate and render icons from SVG files."),
 *   allow_directory = true,
 *   libraries = {
 *     "iconset/svg-icons",
 *   },
 * )
 */
class SvgIconHandler extends IconHandlerFileBase {

  /**
   * {@inheritdoc}
   */
  public function createFileAssets($filepath, $asset_info, IconsetInterface $iconset) {
    try {
      $icons = $this->discoverIcons($filepath, $asset_info);

      return (isset($icons['id']) && is_string($icons['id']))
        ? new ImageAsset($filepath, $icons)
        : new SvgSpriteAsset($filepath, $icons);
    }
    catch (XmlMismatchedTagException $e) {
      $this
        ->getLogger('iconset')
        ->error('Unable to match XML element tags at line @line of @file: @message', [
          '@line' => $e->getLine(),
          '@file' => $e->getFile(),
          '@message' => $e->getMessage(),
        ]);
    }
    catch (\Exception $e) {
      $this
        ->getLogger('iconset')
        ->error('Unable to read SVG file at @line of @file: @message', [
          '@line' => $e->getLine(),
          '@file' => $e->getFile(),
          '@message' => $e->getMessage(),
        ]);
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getFileExtensions() {
    return ['svg'];
  }

  /**
   * {@inheritdoc}
   */
  public function getJsSettings() {
    return [
      'type' => 'svg',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function formatJson(AssetInterface $asset) {
    /** @var \Drupal\iconset\Asset\FileAssetInterface $asset */
    $iconData = [];

    foreach ($asset->getIcons() as $id => $iconInfo) {
      $iconData[] = [
        'id' => $id,
        'label' => $iconInfo['label'],
        'viewBox' => $iconInfo['viewBox'],
      ];
    }

    return [
      'type' => $asset instanceof SvgSpriteAsset ? 'sprite' : 'svg',
      'file' => base_path() . $asset->getFilepath(),
      'icons' => $iconData,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function build($icon_id, AssetInterface $asset, array $options = []) {
    /** @var \Drupal\iconset\Asset\FileAssetInterface $asset */
    $attrs = [];
    $icon = $asset->getIcon($icon_id);
    $url = Url::fromUri('base:' . $asset->getFilepath());

    if (empty($options['decorative'])) {
      $attrs['alt'] = $options['alt'] ?? $icon['label'];
    }
    else {
      $attrs['role'] = 'presentation';
      $attrs['alt'] = '';
    }

    if ($asset instanceof ImageAsset) {
      $attrs['class'] = ['icon', 'icon--svg'];

      // To ensure the least amount of resizing of the HTML element output,
      // apply the image dimensions as best can be calculated. If both
      // dimensions are provided, use them directly, otherwise scale the image.
      if (!empty($options['height'])) {
        $attrs['height'] = intval($options['height']);

        // Scale the width based on the height ratio.
        $attrs['width'] = empty($options['width'])
          ? $icon['width'] * $attrs['height'] / $icon['height']
          : intval($options['width']);
      }
      elseif (!empty($options['width'])) {
        $attrs['width'] = intval($options['width']);
        $attrs['height'] = $icon['height'] * $attrs['width'] / $icon['width'];
      }
      else {
        $attrs['width'] = $icon['width'];
        $attrs['height'] = $icon['height'];
      }

      return [
        '#theme' => 'image',
        '#uri' => $url->toString(),
        '#attributes' => $attrs,
      ];
    }
    else {
      $attrs['viewBox'] = $icon['viewBox'];

      foreach (['width', 'height'] as $optName) {
        if (!empty($options[$optName])) {
          $attrs[$optName] = $options[$optName];
        }
      }

      return [
        '#theme' => 'iconset_svg_icon',
        '#icon' => $url->setOption('fragment', $icon_id),
        '#attributes' => $attrs,
      ];
    }
  }

  /**
   * Read SVG at $filepath and return information about discovered icons.
   *
   * Method parses SVG file to search for symbol definitions for sprite files
   * or general SVG attributes for single SVG files.
   *
   * If XML parsing finds symbols or groups (in a defs node), the file will be
   * treated as if it contains sprites, otherwise the file will be used as if
   * it contains a single SVG and is the image source of the icon.
   *
   * @param string $filepath
   *   The SVG filepath to parse and find icons in.
   * @param string $asset_name
   *   The asset string to identify the asset being searched. The asset is
   *   defined relative to the iconset it is being used for.
   *
   * @return array|null
   *   An array of icon data for SVG icon file or SVG icon sprites. The icon
   *   data must have an ID for every icon, label and viewBox values.
   */
  protected function discoverIcons($filepath, $asset_name) {
    $icons = [];
    $xml = new \XMLReader();

    try {
      $realPath = $this->filesystem->realpath($filepath);

      if ($xml->open($realPath) === FALSE) {
        throw new \InvalidArgumentException('Unable to open SVG file: ' . $filepath);
      }

      if ($this->findXmlElement($xml, 'svg')) {
        $isSprite = FALSE;
        $filename = $this->filesystem->basename($asset_name, '.svg');
        $svgInfo = [
          'id' => $xml->getAttribute('id'),
          'label' => $filename,
          'viewBox' => $xml->getAttribute('viewBox'),
        ];

        while ($xml->read()) {
          if ($xml->nodeType === \XMLReader::ELEMENT && !$xml->isEmptyElement) {
            switch (strtolower($xml->name)) {
              case 'defs':
                // The "def" element can contain things other than symbols and
                // icons, and therefore this element doesn't automatically
                // mean that this is a sprite, unless we find icons.
                if ($addIcons = $this->parseSvgDefs($xml)) {
                  $icons += $addIcons;
                  $isSprite = TRUE;
                }
                break;

              case 'symbol':
                $isSprite = TRUE;

                if ($icon = $this->parseSvgElement($xml)) {
                  $icons[$icon['id']] = $icon;
                }
                break;

              case 'title':
                $svgInfo['label'] = strip_tags($xml->readInnerXML());
                break;

              default:
                $xml->next();
            }
          }
        }

        // If discovered as a SVG file with sprites, then load return a list of
        // discovered <symbols> and top-level <g> elements. Otherwise this is
        // likely to just be a single icon SVG, so return the attributes.
        if ($isSprite) {
          return $icons;
        }
        else {
          if (empty($svgInfo['id'])) {
            $svgInfo['id'] = Html::cleanCssIdentifier($filename);
          }

          // Images don't utilize the viewBox attribute directly, so instead
          // get transform this into a width and height value.
          if ($svgInfo['viewBox']) {
            $dim = explode(' ', $svgInfo['viewBox'], 4);
            $svgInfo['width'] = floatval($dim[2]);
            $svgInfo['height'] = floatval($dim[3]);
          }

          return $svgInfo;
        }
      }
      else {
        throw new \InvalidArgumentException('Unable locate the SVG elements in file: ' . $asset_name);
      }
    }
    finally {
      $xml->close();
    }
  }

  /**
   * Find an XML element tag within the XML document.
   *
   * @param \XMLReader $xml
   *   The XMLReader instance with the loaded XML document.
   * @param string $tag
   *   The XML element tag name to find.
   *
   * @return bool
   *   TRUE if this element was found and the XMLReader is pointed at the
   *   base XML element that was located.
   */
  protected function findXmlElement(\XMLReader $xml, $tag) {
    while ($xml->read()) {
      if ($xml->nodeType === \XMLReader::ELEMENT && strtolower($xml->name) === $tag) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Finds icons inside of a <defs> tag (symbol and g elements).
   *
   * @param \XMLReader $xml
   *   XMLReader instance pointed at the <em>defs</em> element definition.
   *
   * @return array
   *   An array of
   */
  protected function parseSvgDefs(\XMLReader $xml) {
    $icons = [];

    while ($xml->read()) {
      if ($xml->nodeType === \XMLReader::ELEMENT && !$xml->isEmptyElement) {
        switch (strtolower($xml->name)) {
          case 'g':
          case 'symbol':
            if ($icon = $this->parseSvgElement($xml)) {
              $icons[$icon['id']] = $icon;
              break;
            }

          default:
            $xml->next();
        }
      }
      elseif ($xml->nodeType === \XmlReader::END_ELEMENT) {
        if ('defs' !== strtolower($xml->name)) {
          throw new XmlMismatchedTagException($xml->name, 'defs', ['defs']);
        }

        return $icons;
      }
    }

    return $icons;
  }

  /**
   * Parses SVG symbol element into icon information.
   *
   * @param \XMLReader $xml
   *   XMLReader instance pointed at the element definition.
   *
   * @return array|false
   *   Returns icon information if the symbol could be properly parsed and
   *   the required information was extracted. Returns FALSE otherwise.
   */
  protected function parseSvgElement(\XMLReader $xml) {
    $parents = [strtolower($xml->name)];
    $icon = [
      'id' => $xml->getAttribute('id'),
      'viewBox' => $xml->getAttribute('viewBox'),
    ];

    while ($xml->read()) {
      if ($xml->nodeType === \XMLReader::ELEMENT) {
        if ($xml->isEmptyElement) {
          continue;
        }

        $tag = strtolower($xml->name);
        if ($tag === 'title' && count($parents) === 1) {
          $icon['label'] = strip_tags($xml->readInnerXML());
        }

        $parents[] = $tag;
      }
      elseif ($xml->nodeType === \XmlReader::END_ELEMENT) {
        $expected = array_pop($parents);

        if ($expected !== strtolower($xml->name)) {
          throw new XmlMismatchedTagException($xml->name, $expected, $parents);
        }
        elseif (empty($parents)) {
          if (!empty($icon['id'])) {
            $icon['label'] = $icon['label'] ?? $icon['id'];
            return $icon;
          }

          break;
        }
      }
    }

    // Minimally a symbol needs to have an ID to be valid and referenceable.
    return FALSE;
  }

}
