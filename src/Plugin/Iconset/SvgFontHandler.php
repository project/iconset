<?php

namespace Drupal\iconset\Plugin\Iconset;

use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\File\FileSystemInterface;
use Drupal\iconset\IconsetInterface;
use Drupal\iconset\Asset\AssetInterface;
use Drupal\iconset\Asset\FontAsset;
use Drupal\iconset\Plugin\IconHandlerFileBase;
use Drupal\iconset\Xml\XmlMismatchedTagException;

/**
 * Parses font glyphs from SVG files in order to create icon fonts.
 *
 * Handler for icons which are implemented with graphical fonts. At the time of
 * this writing, this handler does capture and search for font information in
 * "font-face" elements in the SVG "font" element, but does not use those to
 * create the necessary font style. Instead the handler assumes a library is
 * already doing the work to bring in those styles, and applying CSS classes
 * to rendered icon HTML will apply the necessary font styles.
 *
 * Configurations for this handler are (defined in *.iconset.yml file):
 * [
 *   'tag' => ( 'i'|'em'|'div' ),
 *   'css_class => {string|string[]},
 *   'font' => [
 *     'family' => {string},
 *     'weight' => {int},
 *   ],
 * ]
 *
 * @todo Add configurations for possible ways to render the icon. For instance
 * don't use the unicode and just add a prefixed icon ID value as a CSS class.
 * This would work for instance with fontawesome with a "fa-" prefix to the
 * icon ID.
 *
 * @IconsetIconHandler(
 *   id = "svg_font",
 *   label = @Translation("SVG Font Glyphs"),
 *   help = @Translation("Enumerate and render fonts from SVG files."),
 *   allow_directory = true,
 *   libraries = {
 *     "iconset/svg-icons",
 *   },
 * )
 */
class SvgFontHandler extends IconHandlerFileBase {

  /**
   * Create a new instance of a icon handler based on a file of icon metadata.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The unique icon handler plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition from the plugin discovery.
   * @param \Drupal\Core\File\FileSystemInterface $filesystem
   *   Filesystem utilities and helper methods.
   * @param \Drupal\Core\Extension\ExtensionPathResolver $extension_path_resolver
   *   The extension path resolver.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FileSystemInterface $filesystem, ExtensionPathResolver $extension_path_resolver) {
    // Apply defaults, "tag" and "css_class" are the the only required needed
    // configruations for rendering these icons.
    $configuration += [
      'tag' => 'i',
      'css_class' => [],
    ];

    // CSS classes should be an array, but could be a single value in the
    // configuration. Convert to an array if configuration is a string value.
    if (!empty($configuration['css_class']) && is_string($configuration['css_class'])) {
      $configuration['css_class'] = [$configuration['css_class']];
    }

    $allowedTags = ['i', 'em', 'div'];
    if (!in_array($configuration['tag'], $allowedTags)) {
      $error = sprintf('Unsupported HTML tag for use with icon fonts, use one of [%s] instead', implode(', ', $allowedTags));
      throw new \InvalidArgumentException($error);
    }

    parent::__construct($configuration, $plugin_id, $plugin_definition, $filesystem, $extension_path_resolver);
  }

  /**
   * {@inheritdoc}
   */
  public function getFileExtensions() {
    return ['svg'];
  }

  /**
   * {@inheritdoc}
   */
  public function createFileAssets($filepath, $asset_info, IconsetInterface $iconset) {
    try {
      $assets = $this->discoverIcons($filepath, $asset_info);

      return !empty($assets) ? $assets : NULL;
    }
    catch (XmlMismatchedTagException $e) {
      $this
        ->getLogger('iconset')
        ->error('Unable to match XML element tags at line @line of @file: @message', [
          '@line' => $e->getLine(),
          '@file' => $e->getFile(),
          '@message' => $e->getMessage(),
        ]);
    }
    catch (\Exception $e) {
      $this
        ->getLogger('iconset')
        ->error('Unable to read SVG file at @line of @file: @message', [
          '@line' => $e->getLine(),
          '@file' => $e->getFile(),
          '@message' => $e->getMessage(),
        ]);
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getJsSettings() {
    $config = $this->configuration;
    $settings = [
      'type' => 'font_icon',
      'tag' => $config['tag'],
      'className' => $config['css_class'],
    ];

    if (!empty($config['font'])) {
      $settings['font'] = $config['font'];
    }

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function formatJson(AssetInterface $asset) {
    $iconData = [];

    foreach ($asset->getIcons() as $id => $icon) {
      $iconData[] = [
        'id' => $id,
        'label' => $icon['label'],
        'unicode' => $icon['unicode'],
      ];
    }

    return [
      'icons' => $iconData,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function build($icon_id, AssetInterface $asset, array $options = []) {
    $config = $this->configuration;
    $icon = $asset->getIcon($icon_id);

    $attrs = [];
    $attrs['class'] = $config['css_class'];

    if (empty($options['decorative'])) {
      $attrs['role'] = 'img';
      $attrs['alt'] = $options['alt'] ?? $icon['label'];
    }
    else {
      $attrs['role'] = 'presentation img';
      $attrs['alt'] = '';
    }

    // The size of icons which are implemented by fonts are determined by the
    // font-size (not width and height). This requires us to do a conversion.
    //
    // @todo Is there a better way to do this?
    if (isset($options['height'])) {
      $attrs['style'] = "font-size: {$options['height']}px;";
    }
    elseif (isset($options['width'])) {
      $attrs['style'] = "font-size: {$options['width']}px;";
    }

    return [
      '#theme' => 'iconset_font_icon',
      '#tag' => $config['tag'],
      '#attributes' => $attrs,
      '#content' => $icon['unicode'],
    ];
  }

  /**
   * Read SVG at $filepath and return information about discovered icons.
   *
   * Method parses SVG file to search for font glyph definitions. Glyphs are
   * expected to be found in the font tags and would have a unicode attribute.
   *
   * @param string $filepath
   *   The SVG filepath to parse and find icons in.
   * @param string $asset_name
   *   The asset string to identify the asset being searched. The asset is
   *   defined relative to the iconset it is being used for.
   *
   * @return \Drupal\iconset\Asset\FontAsset[]|null
   *   An array of icon data for SVG icon file or SVG icon sprites. The icon
   *   data must have an ID for every icon, label and viewBox values.
   */
  protected function discoverIcons($filepath, $asset_name) {
    $fonts = [];
    $xml = new \XMLReader();

    try {
      $realPath = $this->filesystem->realpath($filepath);

      if ($xml->open($realPath) === FALSE) {
        throw new \InvalidArgumentException('Unable to open SVG file: ' . $filepath);
      }

      if ($this->findXmlElement($xml, ['svg', 'defs'])) {
        while ($xml->read()) {
          if ($xml->nodeType === \XMLReader::ELEMENT && !$xml->isEmptyElement) {
            switch (strtolower($xml->name)) {
              case 'font':
                if ($font = $this->parseSvgFont($xml)) {
                  $fonts[] = $font;
                }
                break;

              default:
                $xml->next();
            }
          }
        }

        // If discovered as a SVG file with sprites, then load return a list of
        // discovered <symbols> and top-level <g> elements. Otherwise this is
        // likely to just be a single icon SVG, so return the attributes.
        return $fonts;
      }
      else {
        throw new \InvalidArgumentException('Unable to find Font elements from: ' . $asset_name);
      }
    }
    finally {
      $xml->close();
    }
  }

  /**
   * Find an XML element tag within the XML document.
   *
   * @param \XMLReader $xml
   *   The XMLReader instance with the loaded XML document.
   * @param string|array $tag
   *   The XML element tag name to find. An array can be used to locate a
   *   nested element.
   *
   * @return bool
   *   TRUE if this element was found and the XMLReader is pointed at the
   *   base XML element that was located.
   */
  protected function findXmlElement(\XMLReader $xml, $tag) {
    if (is_array($tag)) {
      $tags = $tag;
      $tag = array_shift($tags);
    }
    else {
      $tags = [];
    }

    while ($xml->read()) {
      if ($xml->nodeType === \XMLReader::ELEMENT && strtolower($xml->name) === $tag) {
        $tag = array_shift($tags);

        if (!$tag) {
          return TRUE;
        }
      }
    }

    return FALSE;
  }

  /**
   * Finds icons inside of a <defs> tag (symbol and g elements).
   *
   * @param \XMLReader $xml
   *   XMLReader instance pointed at the <em>defs</em> element definition.
   *
   * @return \Drupal\iconset\Asset\FontAsset|null
   *   Returned a FontAsset if icon glyphs could be found and read. If no icons
   *   found, NULL will be returned instead.
   *
   * @throws \Drupal\iconset\Xml\XmlMismatchedTagException
   */
  protected function parseSvgFont(\XMLReader $xml) {
    $font = [];
    $icons = [];

    while ($xml->read()) {
      if ($xml->nodeType === \XMLReader::ELEMENT) {
        switch (strtolower($xml->name)) {
          case 'font-face':
            $font = array_filter([
              'font-family' => $xml->getAttribute('font-family'),
              'font-weight' => $xml->getAttribute('font-weight'),
              'font-stretch' => $xml->getAttribute('font-stretch'),
              'unicode-range' => $xml->getAttribute('unicode-range'),
            ]);

            $xml->next();
            break;

          case 'glyph':
            if ($icon = $this->parseSvgGlyph($xml)) {
              $icons[$icon['id']] = $icon;
            }
            break;

          default:
            $xml->next();
        }
      }
      elseif ($xml->nodeType === \XmlReader::END_ELEMENT) {
        if ('font' !== strtolower($xml->name)) {
          throw new XmlMismatchedTagException($xml->name, 'font');
        }

        if ($icons) {
          return new FontAsset($font, $icons);
        }

        return NULL;
      }
    }

    return NULL;
  }

  /**
   * Parses SVG symbol element into icon information.
   *
   * @param \XMLReader $xml
   *   XMLReader instance pointed at the glyph definition.
   *
   * @return array|false
   *   Returns icon information if the symbol could be properly parsed and
   *   the required information was extracted. Returns FALSE otherwise.
   */
  protected function parseSvgGlyph(\XMLReader $xml) {
    $icon = [];
    $icon['id'] = $xml->getAttribute('glyph-name');
    $icon['label'] = $icon['id'];
    $icon['unicode'] = $xml->getAttribute('unicode');

    // Skip the internal content, as all the information about the glyph is
    // at the top. Advance the XML pointer to start at the next element.
    if (!$xml->isEmptyElement) {
      $xml->next();
    }

    // Minimally a symbol needs to have an ID to be valid and referenceable.
    return !empty($icon['id']) ? $icon : FALSE;
  }

}
