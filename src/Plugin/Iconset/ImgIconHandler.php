<?php

namespace Drupal\iconset\Plugin\Iconset;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\ImageToolkit\ImageToolkitManager;
use Drupal\Core\Url;
use Drupal\iconset\IconsetInterface;
use Drupal\iconset\Asset\AssetInterface;
use Drupal\iconset\Asset\ImageAsset;
use Drupal\iconset\Plugin\IconHandlerFileBase;

/**
 * Icon plugin for managing SVG icons and sprite files.
 *
 * Icon plugin handler for reading and determining the contents of an SVG file.
 * SVG files can just be an icon or contain a collection of symbols (sprites).
 *
 * A file with sprites either has "symbol" elements, which each represent icons,
 * or can be "g" elements (groups). Group elements are only considered an icon
 * if they are found in a "defs" element.
 *
 * It is recommend to use SVG sprite files, and to use "symbol" elements for
 * each icon in the sprite. Not only is it more efficient, the icons are also
 * easier to theme, because you can apply CSS properties like fill to them.
 *
 * @IconsetIconHandler(
 *   id = "image",
 *   label = @Translation("SVG icons and sprites"),
 *   help = @Translation("Enumerate and render icons from SVG files."),
 *   allow_directory = true,
 *   libraries = {
 *     "iconset/svg-icons",
 *   },
 * )
 */
class ImgIconHandler extends IconHandlerFileBase {

  /**
   * Image toolkit instance for image manipulation.
   *
   * @var \Drupal\Core\ImageToolkit\ImageToolkitInterface
   */
  protected $imageToolkit;

  /**
   * Create a new instance of the image icon plugin handler.
   *
   * @param array $configuration
   *   Plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\File\FileSystemInterface $filesystem
   *   Filesystem service.
   * @param \Drupal\Core\Extension\ExtensionPathResolver $extension_path_resolver
   *   The extension path resolver.
   * @param \Drupal\Core\ImageToolkit\ImageToolkitManager $image_toolkit_manager
   *   The image toolkit manager to getting an image toolkit instance.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FileSystemInterface $filesystem, ExtensionPathResolver $extension_path_resolver, ImageToolkitManager $image_toolkit_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $filesystem, $extension_path_resolver);

    $this->imageToolkit = $image_toolkit_manager->getDefaultToolkit();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('file_system'),
      $container->get('extension.path.resolver'),
      $container->get('image.toolkit.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFileExtensions() {
    return $this->imageToolkit->getSupportedExtensions();
  }

  /**
   * {@inheritdoc}
   */
  public function createFileAssets($filepath, $asset_info, IconsetInterface $iconset) {
    try {
      $name = $this->filesystem->basename($filepath);
      $name = preg_replace('/\..+$/', '', $name);

      $this->imageToolkit->setSource($filepath);
      $this->imageToolkit->parseFile();

      $icon = [];
      $icon['id'] = Html::cleanCssIdentifier($name);
      $icon['label'] = $name;
      $icon['width'] = $this->imageToolkit->getWidth();
      $icon['height'] = $this->imageToolkit->getHeight();

      return new ImageAsset($filepath, $icon);
    }
    catch (\Exception $e) {
      $this
        ->getLogger('iconset')
        ->error('Unable to read image file at @line of @file: @message', [
          '@line' => $e->getLine(),
          '@file' => $e->getFile(),
          '@message' => $e->getMessage(),
        ]);
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getJsSettings() {
    return [
      'type' => 'image',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function formatJson(AssetInterface $asset) {
    /** @var \Drupal\iconset\Asset\FileAssetInterface $asset */
    $icons = $asset->getIcons();
    $icon = reset($icons);

    return $icon + [
      'file' => base_path() . $asset->getFilepath(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function build($icon_id, AssetInterface $asset, array $options = []) {
    /** @var \Drupal\iconset\Asset\FileAssetInterface $asset */
    $icon = $asset->getIcon($icon_id);
    $url = Url::fromUri('base:' . $asset->getFilepath());

    $attrs = [];
    $attrs['class'] = ['icon', 'icon--img'];

    if (empty($options['decorative'])) {
      $attrs['alt'] = $options['alt'] ?? $icon['label'];
    }
    else {
      $attrs['role'] = 'presentation';
      $attrs['alt'] = '';
    }

    // To ensure the least amount of resizing of the HTML element output,
    // apply the image dimensions as best can be calculated. If both dimensions
    // are provided, use them directly, otherwise scale the image appropriately.
    if (!empty($options['height'])) {
      $attrs['height'] = intval($options['height']);

      // Scale the width based on the height ratio scaling if no explicit width.
      $attrs['width'] = empty($options['width'])
        ? $icon['width'] * $attrs['height'] / $icon['height']
        : intval($options['width']);
    }
    elseif (!empty($options['width'])) {
      $attrs['width'] = intval($options['width']);
      $attrs['height'] = $icon['height'] * $attrs['width'] / $icon['width'];
    }
    else {
      $attrs['width'] = $icon['width'];
      $attrs['height'] = $icon['height'];
    }

    return [
      '#theme' => 'image',
      '#uri' => $url->toString(),
      '#attributes' => $attrs,
    ];
  }

}
