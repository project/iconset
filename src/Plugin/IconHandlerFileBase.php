<?php

namespace Drupal\iconset\Plugin;

use Drupal\Core\Extension\ExtensionPathResolver;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\File\FileSystemInterface;
use Drupal\iconset\IconsetInterface;

/**
 * Base class for icon handlers which parse definition files, to discover icons.
 */
abstract class IconHandlerFileBase extends PluginBase implements IconHandlerFileInterface, ContainerFactoryPluginInterface {

  use LoggerChannelTrait;

  /**
   * Filesystem utilities and helper methods.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $filesystem;

  /**
   * The extension path resolver.
   *
   * @var \Drupal\Core\Extension\ExtensionPathResolver
   */
  protected $extPathResolver;

  /**
   * Create a new instance of a icon handler based on a file of icon metadata.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The unique icon handler plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition from the plugin discovery.
   * @param \Drupal\Core\File\FileSystemInterface $filesystem
   *   Filesystem utilities and helper methods.
   * @param \Drupal\Core\Extension\ExtensionPathResolver $extension_path_resolver
   *   The extension path resolver.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FileSystemInterface $filesystem, ExtensionPathResolver $extension_path_resolver) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->filesystem = $filesystem;
    $this->extPathResolver = $extension_path_resolver;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('file_system'),
      $container->get('extension.path.resolver')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function createAssets($asset_info, IconsetInterface $iconset) {
    try {
      $path = $this->getFilepath($asset_info, $iconset);

      // If not a directory, just handlle and parse as a single file.
      if (!is_dir($path)) {
        return $this->createFileAssets($path, $asset_info, $iconset);
      }

      // Check if this plugin allows the use of directories.
      if (empty($this->pluginDefinition['allow_directory'])) {
        $msg = sprintf('This icon handler (%s) does not support directories as assets.', $this->getPluginId());
        throw new \InvalidArgumentException($msg);
      }

      $extensions = array_map('preg_quote', $this->getFileExtensions(), ['/']);
      $extPattern = '/\.(' . implode('|', $extensions) . ')$/';

      $directory = rtrim($this->getFilepath($asset_info, $iconset), '/');
      $files = $this->filesystem->scanDirectory($directory, $extPattern, [
        'key' => 'basename',
      ]);

      $assets = [];
      foreach ($files as $file) {
        $raw = "/{$file->uri}";
        $path = $this->getFilepath($raw, $iconset);
        $asset = $this->createFileAssets($path, $raw, $iconset);

        if (is_array($asset)) {
          $assets = array_merge($assets, $asset);
        }
        elseif ($asset) {
          $assets[] = $asset;
        }
      }

      return $assets;
    }
    catch (\Exception $e) {
      $this
        ->getLogger('iconset')
        ->error('Unable to directory for SVG files at @line of @file: @message', [
          '@line' => $e->getLine(),
          '@file' => $e->getFile(),
          '@message' => $e->getMessage(),
        ]);
    }

    return NULL;
  }

  /**
   * Convert the file and extension information into a filepath to load icons.
   *
   * @param string $uri
   *   The asset information to use when trying to resolve the filepath of the
   *   file asset containing icons to search in.
   * @param \Drupal\iconset\IconsetInterface $iconset
   *   The iconset plugin to get the asset from.
   *
   * @return string
   *   Name of the file relative to the extension path, ready for direct usage
   *   with the file commands.
   *
   * @throws \InvalidArgumentException
   *   If extension (module or theme) or asset file doesn't exist.
   */
  protected function getFilepath($uri, IconsetInterface $iconset) {
    // Either a absolute path (from Drupal root) or URI (streamwrapper).
    if (preg_match('#^(\w+://|\.?/)#', $uri)) {
      $filepath = ltrim($uri, './');
    }
    else {
      $filepath = $this->extPathResolver->getPath($iconset->getProviderType(), $iconset->getProvider()) . '/' . $uri;
    }

    if (!file_exists($filepath)) {
      throw new \InvalidArgumentException('Invalid asset URI: /' . $uri);
    }

    return $filepath;
  }

}
