<?php

namespace Drupal\iconset\Plugin\Field\FieldWidget;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\iconset\Element\IconsetSelector;
use Drupal\link\Plugin\Field\FieldWidget\LinkWidget;
use Drupal\link_attributes\LinkAttributesManager;
use Drupal\iconset\IconsetManagerInterface;

/**
 * Plugin implementation of the 'link field' widget with iconset support.
 *
 * @FieldWidget(
 *   id = "iconset_link",
 *   label = @Translation("Link with icon"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class IconLinkWidget extends LinkWidget implements ContainerFactoryPluginInterface, TrustedCallbackInterface {

  /**
   * The iconset plugin manager.
   *
   * @var \Drupal\iconset\IconsetManagerInterface
   */
  protected $iconsetManager;

  /**
   * The link attributes manager, if the plugin manager is available.
   *
   * @var \Drupal\link_attributes\LinkAttributesManager|null
   */
  protected $attributesManager;

  /**
   * Constructs an IconLinkFieldWidget instance.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\iconset\IconsetManagerInterface $iconset_manager
   *   The iconset plugin manager.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, IconsetManagerInterface $iconset_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);

    $this->iconsetManager = $iconset_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('strategy.manager.iconset')
    );

    // Only set this plugin manager if the link_attributes module is enabled
    // and the plugin manager is available. This is an optional dependency.
    $linkAttributesManager = $container->get('plugin.manager.link_attributes', ContainerInterface::NULL_ON_INVALID_REFERENCE);
    if ($linkAttributesManager) {
      $instance->setAttributesManager($linkAttributesManager);
    }

    return $instance;
  }

  /**
   * Set the optional attributes plugin manager if link_attributes is enabled.
   *
   * If link_attributes module is available, it adds the provides the ability
   * to configure additional attributes for the link. This improves the control
   * over the link formatting and rendering. We want to leverage it when
   * link_attributes are available but never require it.
   *
   * @param \Drupal\link_attributes\LinkAttributesManager $attributes_manager
   *   The link attributes manager to use for attribute link options.
   */
  public function setAttributesManager(LinkAttributesManager $attributes_manager) {
    $this->attributesManager = $attributes_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['widgetElementPreRender'];
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'iconset' => [],
      'enabled_attributes' => [
        'target' => 'target',
        'rel' => 'rel',
        'class' => 'class',
      ],
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function form(FieldItemListInterface $items, array &$form, FormStateInterface $form_state, $get_delta = NULL) {
    $elements = parent::form($items, $form, $form_state, $get_delta);
    $elements['#attached']['library'][] = 'iconset/link-widget';

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    /** @var \Drupal\Core\Field\FieldItemInterface $item */
    $item = $items[$delta];
    $options = $item->get('options')->getValue();
    $allowedIconsets = $this->getSetting('iconset');

    $element['icon'] = [
      '#type' => 'iconset_selector',
      '#title' => $this->t('Icon'),
      '#format' => IconsetSelector::COMPACT_FORMAT,
      '#iconset' => $allowedIconsets,
      '#default_value' => $options['icon'] ?? NULL,
      '#parents' => array_merge($element['#field_parents'], [
        $this->fieldDefinition->getName(),
        $delta,
        'options',
        'icon',
      ]),
      '#weight' => -5,
    ];

    // Apply special styling for when only a single iconset is available.
    if (count($allowedIconsets) == 1) {
      $element['#pre_render'][] = static::class . '::widgetElementPreRender';
    }

    // If links attributes are available, allow apply attributes to the links.
    if ($this->attributesManager) {
      $plugin_definitions = $this->attributesManager->getDefinitions();
      $enabled_attributes = $this->getSetting('enabled_attributes');

      // Only create attributes configurations if any of the link_attributes
      // are enabled. Otherwise we can skip generating these.
      if ($attributes = array_intersect_key($plugin_definitions, $enabled_attributes)) {
        $attributeValues = $options['attributes'] ?? [];
        $element['options']['attributes'] = [
          '#type' => 'details',
          '#title' => $this->t('Attributes'),
          '#tree' => TRUE,
          '#open' => TRUE,
        ];

        foreach ($attributes as $name => $definition) {
          foreach ($definition as $property => $value) {
            // Don't set ID with the plugin ID.
            if ($property !== 'id') {
              $element['options']['attributes'][$name]['#' . $property] = $value;
            }
          }

          // Use the Toolshed for the classname attributes. This provides better
          // validation and automatic conversion, to and from the array values.
          if ($name === 'class') {
            $element['options']['attributes'][$name]['#type'] = 'css_class';
          }

          // Set the default value if available.
          $element['options']['attributes'][$name]['#default_value'] = $attributeValues[$name] ?? NULL;
        }
      }
    }

    return $element;
  }

  /**
   * Update the widget form element with styling and container wrapping updates.
   *
   * @param array $element
   *   The form element for the widget to prepare for rendering.
   *
   * @return array
   *   The updated form element for rendering.
   */
  public static function widgetElementPreRender(array $element) {
    $container = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['inline-link-widget'],
      ],

      'icon' => $element['icon'],
      'link_widget' => [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['inline-link-widget__wrapper'],
        ],
      ],
    ];

    // Update icon selector styles and remove from top level widget.
    $container['icon']['#title_display'] = 'hidden';
    $container['icon']['#attributes']['class'][] = 'inline-link-widget__icon-selector';
    unset($element['icon']);

    // Move the other form elements into the wrapper so it can be
    // properly styled to flexbox with the icon selector.
    foreach (Element::children($element) as $key) {
      if (!empty($element[$key])) {
        $container['link_widget'][$key] = $element[$key];
        unset($element[$key]);
      }
    }

    $element['container'] = $container;
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['iconset'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Allowed iconsets'),
      '#options' => $this->iconsetManager->getIconsets(),
      '#default_value' => $this->getSetting('iconset'),
      '#element_validate' => [static::class . '::validateCheckboxSettings'],
    ];

    if ($this->attributesManager) {
      $options = array_column($this->attributesManager->getDefinitions(), 'title', 'id');
      $form['enabled_attributes'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Enabled attributes'),
        '#options' => $options,
        '#default_value' => $this->getSetting('enabled_attributes'),
        '#element_validate' => [static::class . '::validateCheckboxSettings'],
        '#description' => $this->t('Select the attributes to allow the user to edit.'),
      ];
    }

    return $form;
  }

  /**
   * Form element "element_validate" callback to filter checkbox items.
   *
   * Checkboxes form element keeps all keys but sets the empty values with
   * FALSE. Most of form processing allows us to clean out the empty value in
   * a submit callback, but this is tougher with widget settings.
   *
   * @param array $element
   *   The checkboxes form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state, values and build information.
   * @param array $complete_form
   *   Reference to the entire form being validated.
   */
  public static function validateCheckboxSettings(array &$element, FormStateInterface $form_state, array &$complete_form) {
    if ($element['#type'] === 'checkboxes') {
      $values = $form_state->getValue($element['#parents']);
      $form_state->setValue($element['#parents'], array_filter($values));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setSettings(array $settings) {
    $settings['enabled_attributes'] = array_filter($settings['enabled_attributes']);
    $settings['iconset'] = array_filter($settings['iconset']);

    return parent::setSettings($settings);
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    // Convert a class string to an array so that it can be merged reliable.
    foreach ($values as &$value) {
      if (!empty($value['options']['attributes'])) {
        $value['options']['attributes'] = array_filter($value['options']['attributes']);
      }
    }
    unset($value);

    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    // Enabled iconsets for the link.
    if ($allowed_sets = $this->getSetting('iconset')) {
      $iconsetLabels = $this->iconsetManager->getIconsets();
      $summary[] = $this->t('Iconsets: @iconsets', [
        '@iconsets' => implode(', ', array_intersect_key($iconsetLabels, $allowed_sets)),
      ]);
    }
    else {
      $summary[] = $this->t('All iconsets');
    }

    // Report the enabled link attributes.
    if ($this->attributesManager && $this->getSetting('enabled_attributes')) {
      $summary[] = $this->t('With attributes: @attributes', [
        '@attributes' => implode(', ', $this->getSetting('enabled_attributes')),
      ]);
    }

    return $summary;
  }

}
