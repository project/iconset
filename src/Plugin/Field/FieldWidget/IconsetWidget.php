<?php

namespace Drupal\iconset\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Default field widget for selecting an icon.
 *
 * @FieldWidget(
 *   id = "iconset_icon_widget",
 *   label = @Translation("Icon selector"),
 *   field_types = {
 *     "iconset_icon"
 *   }
 * )
 */
class IconsetWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $item = $items[$delta];
    $iconsets = $this->getFieldSetting('allowed_iconsets') ?? [];

    $value = $item->getValue();
    $element['icon'] = [
      '#type' => 'iconset_selector',
      '#title' => $element['#title'],
      '#required' => $element['#required'] ?? FALSE,
      '#iconset' => $iconsets,
      '#default_value' => $value,
    ];

    if (!$this->getFieldSetting('decorative') && $this->getFieldSetting('alt_text')) {
      $element['alt_text'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Alt text'),
        '#maxlength' => 255,
        '#default_value' => $value['alt'] ?? '',
      ];
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $items = [];
    foreach ($values as $delta => $value) {
      $items[$delta] = $value['icon'];

      if (isset($value['alt_text'])) {
        $items[$delta]['alt'] = $value['alt_text'];
      }
    }

    return $items;
  }

}
