<?php

namespace Drupal\iconset\Plugin\Field\FieldFormatter;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\link\Plugin\Field\FieldFormatter\LinkFormatter;
use Drupal\iconset\IconsetManagerInterface;
use Drupal\iconset\Exception\IconNotFoundException;

/**
 * A field formatter for displaying Iconset icon in link field content.
 *
 * @FieldFormatter(
 *   id = "iconset_link",
 *   label = @Translation("Icon link"),
 *   field_types = {
 *     "link",
 *   },
 * )
 */
class IconLinkFormatter extends LinkFormatter {

  /**
   * The iconset manager.
   *
   * @var \Drupal\iconset\IconsetManagerInterface
   */
  protected $iconsetManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var \Drupal\iconset\Plugin\Field\FieldFormatter\IconLinkFormatter */
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);

    // Add the iconset manager service, without being aware of or dependent on
    // LinkFormatter services. This means that LinkFormatter can be updated
    // without the this class needing to be updated.
    $iconsetManager = $container->get('strategy.manager.iconset');
    $instance->setIconsetManager($iconsetManager);

    return $instance;
  }

  /**
   * Method to set the iconset manager for this formatter instance.
   *
   * Generally used in Symfony services.yml "calls" service definitions to
   * set dependencies conditionally or with method calls instead of with
   * constructor arguments.
   *
   * @param \Drupal\iconset\IconsetManagerInterface $iconset_manager
   *   The iconset manager.
   */
  public function setIconsetManager(IconsetManagerInterface $iconset_manager) {
    $this->iconsetManager = $iconset_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'style' => 'icon_text',
      'width' => '',
      'height' => '',
      'css_class' => [],
    ] + parent::defaultSettings();
  }

  /**
   * Get the icon rendering style options available to the link formatter.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup[]
   *   An array of options for rendering style of the link title and icon.
   */
  public function getDisplayStyles() {
    return [
      'icon_text' => $this->t('Icon with title'),
      'icon' => $this->t('Icon only'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['style'] = [
      '#type' => 'select',
      '#title' => $this->t('Icon display style'),
      '#required' => TRUE,
      '#options' => $this->getDisplayStyles(),
      '#default_value' => $this->getSetting('style'),
    ];

    $elements['width'] = [
      '#type' => 'number',
      '#title' => $this->t('Width (px)'),
      '#min' => 1,
      '#default_value' => $this->getSetting('width'),
      '#description' => $this->t('The width attribute to set for this icon, will be overridden by CSS, and will auto size to maintain aspect ratio if height is set and this is left blank.'),
    ];

    $elements['height'] = [
      '#type' => 'number',
      '#title' => $this->t('Height (px)'),
      '#min' => 1,
      '#default_value' => $this->getSetting('height'),
      '#description' => $this->t('The height attribute to set for this icon, will be overridden by CSS, and will auto size to maintain aspect ratio if width is set and this is left blank.'),
    ];

    $elements['css_class'] = [
      '#type' => 'css_class',
      '#title' => $this->t('Additional CSS classes'),
      '#default_value' => $this->getSetting('css_class'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    $displayStyles = $this->getDisplayStyles();
    $summary[] = $this->t('Icon style: @style', [
      '@style' => $displayStyles[$this->getSetting('style')] ?? 'None',
    ]);

    $width = $this->getSetting('width');
    $height = $this->getSetting('height');
    if ($width) {
      if ($height) {
        $summary[] = $this->t('Icon size (pixels): %width x %height', [
          '%width' => $width,
          '%height' => $height,
        ]);
      }
      else {
        $summary[] = $this->t('Icon width: %width px', ['%width' => $width]);
      }
    }
    elseif ($height) {
      $summary[] = $this->t('Icon height: %height px', ['%height' => $height]);
    }

    if ($classes = $this->getSetting('css_class')) {
      $summary[] = $this->t('Icon classes: %css_names', [
        '%css_names' => implode(' ', $classes),
      ]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);

    $iconOptions = array_filter([
      'width' => $this->getSetting('width'),
      'height' => $this->getSetting('height'),
    ]);

    // Render only the icon in place of the title text.
    $buildMethod = $this->getSetting('style') === 'icon' ? 'buildIconOnly' : 'buildIconAndText';

    foreach ($items as $delta => $item) {
      $icon = $item->options['icon'] ?? NULL;

      if (!empty($elements[$delta]) && $icon) {
        try {
          if (is_string($icon)) {
            $parts = explode(':', $icon, 2);
            $icon = [
              'iconset' => $parts[0],
              'icon' => $parts[1] ?? '',
            ];
          }

          $elements[$delta]['#title'] = $this->{$buildMethod}($elements[$delta], $icon, $iconOptions);
          $elements[$delta]['#attributes']['class'][] = 'icon-link';
        }
        catch (PluginException | IconNotFoundException $e) {
          // Missing iconset or icon, skip altering the output of this link.
        }
      }
    }

    return $elements;
  }

  /**
   * Builds the title as just an icon, and uses the title as the icon alt text.
   *
   * @param array $element
   *   The link renderable element.
   * @param array $icon
   *   An array with the iconset, and icon data.
   * @param array $options
   *   Icon build options.
   *
   * @return array
   *   Renderable array with content to use as the link title.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *   Missing iconset, or other problem retrieving the iconset requested.
   * @throws \Drupal\iconset\Exception\IconNotFoundException
   *   If requested icon is not available or part of the specified iconset.
   */
  protected function buildIconOnly(array $element, array $icon, array $options = []) {
    $options['decorative'] = FALSE;
    $options['alt'] = $element['#title'] ?? NULL;

    /** @var \Drupal\iconset\IconsetInterface $iconset */
    $iconset = $this->iconsetManager->getInstance($icon['iconset']);
    $build = $iconset->build($icon['icon'], $options);

    // Apply additional class names to the icon render attributes.
    if ($classNames = $this->getSetting('css_class')) {
      $build['#attributes']['class'] = empty($build['#attributes']['class'])
        ? $classNames : array_merge($classNames, $build['#attributes']['class']);
    }

    return $build;
  }

  /**
   * Builds the title as an icon with text.
   *
   * @param array $element
   *   The link renderable element.
   * @param array $icon
   *   An array with the iconset, and icon data.
   * @param array $options
   *   Icon build options.
   *
   * @return array
   *   Renderable array with content to use as the link title.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *   Missing iconset, or other problem retrieving the iconset requested.
   * @throws \Drupal\iconset\Exception\IconNotFoundException
   *   If requested icon is not available or part of the specified iconset.
   */
  protected function buildIconAndText(array $element, array $icon, array $options = []) {
    $options['decorative'] = TRUE;

    /** @var \Drupal\iconset\IconsetInterface $iconset */
    $iconset = $this->iconsetManager->getInstance($icon['iconset']);
    $build = $iconset->build($icon['icon'], $options);

    // Apply additional class names to the icon render attributes.
    $build['#attributes']['class'][] = 'icon-link__icon';
    if ($classNames = $this->getSetting('css_class')) {
      $build['#attributes']['class'] = array_merge($classNames, $build['#attributes']['class']);
    }

    $text = [];
    if ($element['#title']) {
      $text = [
        '#type' => 'html_tag',
        '#tag' => 'span',
        '#attributes' => [
          'class' => ['icon-link__text'],
        ],

        // Checks if render array or it string / markup content needs to get
        // wrapped into a render array.
        'content' => is_array($element['#title']) ? $element['#title'] : [
          '#plain_text' => $element['#title'],
        ],
      ];
    }

    return [
      'icon' => $build,
      'text' => $text,
    ];
  }

}
