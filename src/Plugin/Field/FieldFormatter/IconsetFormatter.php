<?php

namespace Drupal\iconset\Plugin\Field\FieldFormatter;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\iconset\Exception\IconNotFoundException;
use Drupal\iconset\IconsetManagerInterface;

/**
 * A field formatter for displaying Iconset icons.
 *
 * @FieldFormatter(
 *   id = "iconset_icon_formatter",
 *   label = @Translation("Rendered icon"),
 *   field_types = {
 *     "iconset_icon",
 *   },
 * )
 */
class IconsetFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The iconset manager.
   *
   * @var \Drupal\iconset\IconsetManagerInterface
   */
  protected $iconsetManager;

  /**
   * Constructs an IconsetFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   * @param array $settings
   *   The formatter settings.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup|string $label
   *   The field label.
   * @param string $view_mode
   *   The view mode machine name.
   * @param array $third_party_settings
   *   Third party settings for the field formatter.
   * @param \Drupal\iconset\IconsetManagerInterface $iconset_manager
   *   The iconset manager.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, IconsetManagerInterface $iconset_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);

    $this->iconsetManager = $iconset_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('strategy.manager.iconset')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'width' => '',
      'height' => '',
      'css_class' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);
    $settings = $this->getSettings();

    $elements['width'] = [
      '#type' => 'number',
      '#title' => $this->t('Width (px)'),
      '#min' => 1,
      '#default_value' => $settings['width'] ?? NULL,
      '#description' => $this->t('The width attribute to set for this icon, will be overridden by CSS, and will auto size to maintain aspect ratio if height is set and this is left blank.'),
    ];

    $elements['height'] = [
      '#type' => 'number',
      '#title' => $this->t('Height (px)'),
      '#min' => 1,
      '#default_value' => $settings['height'] ?? NULL,
      '#description' => $this->t('The height attribute to set for this icon, will be overridden by CSS, and will auto size to maintain aspect ratio if width is set and this is left blank.'),
    ];

    $elements['css_class'] = [
      '#type' => 'css_class',
      '#title' => $this->t('Additional CSS classes'),
      '#default_value' => $settings['css_class'] ?? [],
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $settings = $this->getSettings();

    if (!empty($settings['width'])) {
      if (!empty($settings['height'])) {
        $summary[] = $this->t('Icon size (pixels): %width x %height', [
          '%width' => $settings['width'],
          '%height' => $settings['height'],
        ]);
      }
      else {
        $summary[] = $this->t('Icon width: %width px', [
          '%width' => $settings['width'],
        ]);
      }
    }
    elseif (!empty($settings['height'])) {
      $summary[] = $this->t('Icon height: %height px', [
        '%height' => $settings['height'],
      ]);
    }

    if (!empty($settings['css_class'])) {
      $summary[] = $this->t('CSS Classes: %css_names', [
        '%css_names' => implode(' ', $settings['css_class']),
      ]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    // Fetch the width and height settings, and prepare them to be applied
    // to the icon rendering options.
    $defaults = array_filter([
      'width' => $this->getSetting('width'),
      'height' => $this->getSetting('height'),
    ]);

    if ($this->getFieldSetting('decorative')) {
      $defaults['decorative'] = 'true';
      $defaults['alt'] = '';
      $hasAltText = FALSE;
    }
    else {
      $hasAltText = $this->getFieldSetting('alt_text');
    }

    $elements = [];
    foreach ($items as $delta => $item) {
      $options = $defaults;

      if ($hasAltText && $item->alt) {
        $options['alt'] = $item->alt;
      }

      try {
        /** @var \Drupal\iconset\IconsetInterface $iconset */
        $iconset = $this->iconsetManager->getInstance($item->iconset);
        $build = $iconset->build($item->icon, $options);

        if ($classNames = $this->getSetting('css_class')) {
          $build['#attributes']['class'] = empty($build['#attributes']['class'])
            ? $classNames : array_merge($classNames, $build['#attributes']['class']);
        }

        $elements[$delta] = $build;
      }
      catch (PluginException | IconNotFoundException $e) {
        $elements[$delta] = [];
      }
    }

    return $elements;
  }

}
