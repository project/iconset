<?php

namespace Drupal\iconset\Plugin\Field\FieldType;

use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\iconset\IconsetManagerInterface;

/**
 * Field to represent icons from an iconset.
 *
 * @FieldType(
 *   id = "iconset_icon",
 *   label = @Translation("Iconset icon"),
 *   description = @Translation("Select an icon from defined iconsets."),
 *   default_widget = "iconset_icon_widget",
 *   default_formatter = "iconset_icon_formatter",
 * )
 */
class IconsetFieldItem extends FieldItemBase {

  /**
   * Plugin manager for iconset discovery and definitions.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $iconsetManager;

  /**
   * Get the Iconset plugin manager.
   *
   * @return \Drupal\iconset\IconsetManagerInterface
   *   Plugin manager for iconset discovery and definitions.
   */
  protected function getIconsetManager(): IconsetManagerInterface {
    if (!isset($this->iconsetManager)) {
      $this->iconsetManager = \Drupal::service('strategy.manager.iconset');
    }

    return $this->iconsetManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return 'icon';
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'alt_text' => TRUE,
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'allowed_iconsets' => [],
      'decorative' => FALSE,
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = [];

    $properties['iconset'] = DataDefinition::create('string')
      ->setLabel(t('Decision Tree'))
      ->setRequired(TRUE);

    $properties['icon'] = DataDefinition::create('string')
      ->setLabel(t('Configuration'))
      ->setRequired(TRUE);

    $properties['alt'] = DataDefinition::create('string')
      ->setLabel(t('Icon alt text'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $columns = [
      'iconset' => [
        'description' => 'The iconset the selected icon belongs to.',
        'type' => 'varchar_ascii',
        'length' => 64,
        'not null' => TRUE,
      ],
      'icon' => [
        'description' => 'The identifier of the icon.',
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
      ],
    ];

    if ($field_definition->getSetting('alt_text')) {
      $columns['alt'] = [
        'description' => 'Alt text for this icon.',
        'type' => 'varchar',
        'length' => 255,
      ];
    }

    return ['columns' => $columns];
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $elements['alt_text'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow custom alt text.'),
      '#default_value' => $this->getSetting('alt_text'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $setOptions = $this->getIconsetManager()->getIconsets();
    $elements = [
      'allowed_iconsets' => [
        '#type' => 'checkboxes',
        '#title' => $this->t('Allowed iconsets'),
        '#required' => TRUE,
        '#options' => $setOptions,
        '#default_value' => $this->getSetting('allowed_iconsets'),
      ],
      'decorative' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Is icon decorative or presentational?'),
        '#default_value' => $this->getSetting('decorative'),
        '#description' => $this->t('Use when the icons are used for decorative purposes and do not need to announce on assistive technologies.<br /> This option disables the "alt" attribute and sets an aria-role of "presentation" to rendered icons.'),
      ],
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function fieldSettingsToConfigData(array $settings) {
    $settings['allowed_iconsets'] = array_filter($settings['allowed_iconsets']);
    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return empty($this->iconset) || empty($this->icon);
  }

}
