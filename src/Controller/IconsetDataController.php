<?php

namespace Drupal\iconset\Controller;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\iconset\IconsetInterface;

/**
 * Iconset JSON data fetching endpoints for getting information about iconsets.
 *
 * Return data about iconsets that are consumable by Javascript and their
 * respective Javascript handlers.
 */
class IconsetDataController {

  /**
   * Fetch the JSON data for the provided iconset plugin.
   *
   * @param \Drupal\iconset\IconsetInterface $iconset
   *   A loaded iconset plugin.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   An HTTP response with the iconset data in JSON format.
   */
  public static function fetchJsonData(IconsetInterface $iconset) {
    $response = new CacheableJsonResponse($iconset->fetchJson());

    $cacheableMetaData = new CacheableMetaData();
    $cacheableMetaData->setCacheTags(['iconset.list']);
    $cacheableMetaData->setCacheContexts(['route']);
    $response->addCacheableDependency($cacheableMetaData);

    return $response;
  }

}
