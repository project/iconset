# Custon Iconsets

*IN ACTIVE DEVELOPMENT* not yet functional - but coming soon? maybe? ish?

_eh, all depends if I have time for it?_


## Description

Extends the *Iconset* module functionality to allow for administrators to create
and manage custom iconsets. Custom iconsets are content entities, with uploaded
asset files.


## Exportability

Unlike the base *Iconset* functionality, where the definitions and assets live
with the providing module or theme, custom iconsets rely on upload file assets
and are implemented with content entities. This means that they cannot be
exported moved and managed with Drupal configurations.

In order to move custom iconsets, content data and file synchronization need to
happen. If icons are referencing either content or file assets that are missing,
the icons will render blank.

If you have iconset that require more reliable availability, then consider
moving your iconset definitions and assets into libraries, modules or themes and
make them available with the base *Iconset* handling.


## Security

Allowing users to upload SVG (and other icon files) files poses some security
risks. The contents of a file may contain dangerous XML / HTML that could get
included on pages when an icon is requested and the resources are included.

There are plans to add utilities to extract and build sprite SVGs from the user
uploaded resource files. This process can help mitigate the risk but this does
not completely ensure the results will always be secure.

We will always attempt to provide the security where we can, however,
precautions should always be taken, such as *only allowing trusted sources* to
create, manage and upload iconsets.
