<?php

namespace Drupal\iconset_menu\Routing;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Subscriber for menu icon setting routes.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new RouteSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_manager) {
    $this->entityTypeManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    $menu = $this->entityTypeManager->getDefinition('menu');

    // Only when Menu UI module is enabled and providing the manage menu
    // routes and forms, should the icon configuration forms also be available.
    if ($menu && $editRoute = $collection->get('entity.menu.edit_form')) {
      $route = new Route($editRoute->getPath() . '/iconset');
      $route->setDefaults([
        '_form' => '\Drupal\iconset_menu\Form\IconsetMenuSettings',
        '_title' => 'Menu Icon Settings',
      ]);

      $adminPermission = $menu->getAdminPermission();
      if ($adminPermission) {
        $route->setRequirement('_permission', $adminPermission);
      }
      else {
        $route->setRequirements($editRoute->getRequirements());
      }

      $route->setOption('parameters', $editRoute->getOption('parameters') ?: [
        'menu' => ['type' => 'entity:menu'],
      ]);

      $collection->add('entity.menu.iconset_config', $route);
    }
  }

}
