<?php

namespace Drupal\iconset_menu;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Template\AttributeHelper;
use Drupal\system\Entity\Menu;
use Drupal\menu_link_content\Plugin\Menu\MenuLinkContent;
use Drupal\iconset\Exception\IconNotFoundException;
use Drupal\iconset\IconsetManagerInterface;

/**
 * Utility for formatting menu items with icon styles applied.
 */
class IconsetMenuFormatter {

  use StringTranslationTrait;

  const MENU_NO_ICON = 'none';
  const MENU_ICON_TEXT = 'icon_text';
  const MENU_ICON_ONLY = 'icon_only';

  /**
   * The menu entity storage handler.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $menuStorage;

  /**
   * The plugin manager for managing iconsets.
   *
   * @var \Drupal\iconset\IconsetManagerInterface
   */
  protected $iconsetManager;

  /**
   * The elements renderer object.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Create a new instance of the IconsetMenuFormatter.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\iconset\IconsetManagerInterface $iconset_manager
   *   The plugin manager responsible for fetching and discovering icons.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The elements renderer object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, IconsetManagerInterface $iconset_manager, RendererInterface $renderer) {
    $this->menuStorage = $entity_type_manager->getStorage('menu');
    $this->iconsetManager = $iconset_manager;
    $this->renderer = $renderer;
  }

  /**
   * Get a list of available menu formatter styles for rendering menu items.
   *
   * @return array
   *   An array keyed by the icon formatting style available for rendering
   *   menus with this formatter.
   */
  public function getStyles() {
    return [
      self::MENU_NO_ICON => [
        'label' => $this->t('No icons'),
      ],
      self::MENU_ICON_TEXT => [
        'label' => $this->t('Icons with text'),
        'callback' => [$this, 'formatIconAndText'],
        'css_class' => [
          'menu--icon',
          'menu--icon-text',
        ],
      ],
      self::MENU_ICON_ONLY => [
        'label' => $this->t('Display icons only'),
        'callback' => [$this, 'formatIconOnly'],
        'css_class' => [
          'menu--icon',
          'menu--icon-only',
        ],
      ],
    ];
  }

  /**
   * Fetch the iconset menu rendering settings.
   *
   * @param \Drupal\system\Entity\Menu|string $menu
   *   Either the menu entity or the menu entity ID.
   *
   * @return array
   *   The iconset menu style settings, and allowed iconsets.
   */
  public function getMenuSettings($menu) {
    if (is_string($menu)) {
      $menu = $this->menuStorage->load($menu);
    }

    $defaultSettings = [
      'icon_style' => self::MENU_NO_ICON,
      'allowed_iconsets' => [],
      'css_class' => [],
    ];

    if ($menu instanceof Menu) {
      return $menu->getThirdPartySettings('iconset') + $defaultSettings;
    }

    return $defaultSettings;
  }

  /**
   * Apply the menu formatter style selected for the menu.
   *
   * @param \Drupal\system\Entity\Menu|string $menu
   *   Either the menu entity or the menu entity ID.
   * @param array $items
   *   The individual menu items to alter with icon formatting styles.
   * @param array $attributes
   *   Menu attributes to alter if appropriate for the style.
   */
  public function format($menu, array &$items, array &$attributes = []) {
    $settings = $this->getMenuSettings($menu);
    $styles = $this->getStyles();

    if (!empty($styles[$settings['icon_style']])) {
      $style = $styles[$settings['icon_style']];

      if (isset($style['callback']) && is_callable($style['callback'])) {
        $dimensions = [];

        if (!empty($settings['width'])) {
          $dimensions['width'] = $settings['width'];
        }

        if (!empty($settings['height'])) {
          $dimensions['height'] = $settings['height'];
        }

        // Generate the menu items with icons based on the configure style.
        $style['callback']($items, $dimensions, [
          'class' => $settings['css_class'],
        ]);
      }

      if (!empty($style['css_class'])) {
        $attributes['class'] = array_merge($style['css_class'], $attributes['class'] ?? []);
      }
    }
  }

  /**
   * Find the icon values and settings for a menu link entity.
   *
   * @param \Drupal\menu_link_content\Plugin\Menu\MenuLinkContent $menu_link
   *   The menu link content entity to find the icon values for.
   *
   * @return array|null
   *   An array with the iconset and icon values for the icon to render or NULL
   *   if either no icon, or unable to find the icon.
   */
  protected static function getIcon(MenuLinkContent $menu_link) {
    $linkOpts = $menu_link->getOptions();

    if (!empty($linkOpts['iconset'])) {
      // If a string, then parse the compact iconset storage format.
      if (is_string($linkOpts['iconset'])) {
        $parts = explode(':', $linkOpts['iconset'], 2);

        return [
          'iconset' => $parts[0],
          'icon' => $parts[1],
        ];
      }

      return $linkOpts['iconset'];
    }

    return NULL;
  }

  /**
   * Alter menu items to have the icon and title display.
   *
   * @param array $items
   *   Reference to the array of menu items to alter.
   * @param array $options
   *   Options to apply the the icon build. This would include height and width
   *   parameters for rendering the dimensions.
   * @param array $attributes
   *   Attributes array to apply to the icon rendering.
   */
  public function formatIconAndText(array &$items, array $options = [], array $attributes = []) {
    foreach ($items as &$item) {
      $icon = $item['original_link'] instanceof MenuLinkContent
        ? static::getIcon($item['original_link']) : NULL;

      if ($icon) {
        try {
          /** @var \Drupal\iconset\IconsetInterface $iconset */
          $iconset = $this->iconsetManager->getInstance($icon['iconset']);
          $options['decorative'] = TRUE;

          $build = [
            'icon' => $iconset->build($icon['icon'], $options),
            'text' => is_array($item['title']) ? $item['title'] : ['#plain_text' => $item['title']],
          ];

          $build['icon']['#attributes'] = isset($build['icon']['#attributes'])
            ? AttributeHelper::mergeCollections($build['icon']['#attributes'], $attributes)
            : $attributes;

          $item['title'] = $this->renderer->renderPlain($build);
          $item['has_icon'] = TRUE;
        }
        catch (PluginException | PluginNotFoundException | IconNotFoundException $e) {
          // Don't render any icons if unable to fetch or build the requested
          // icon. This should leave the menu title unaltered, and will render
          // menu item normally.
        }
      }

      if ($item['below']) {
        $this->formatIconAndText($item['below'], $options, $attributes);
      }
    }
  }

  /**
   * Format the menu items to be only an icon, when the icon is avaiable.
   *
   * @param array $items
   *   Reference to the array of menu items to alter.
   * @param array $options
   *   Options to apply the the icon build. This would include height and width
   *   parameters for rendering the dimensions.
   * @param array $attributes
   *   Attributes array to apply to the icon rendering.
   */
  public function formatIconOnly(array &$items, array $options = [], array $attributes = []) {
    foreach ($items as &$item) {
      $icon = $item['original_link'] instanceof MenuLinkContent
        ? static::getIcon($item['original_link']) : NULL;

      if ($icon) {
        $title = is_array($item['title']) ? $this->renderer->renderPlain($item['title']) : $item['title'];
        $urlAttrs = $item['url']->getOption('attributes');
        $urlAttrs['title'] = $title;
        $item['url']->setOption('attributes', $urlAttrs);

        try {
          /** @var \Drupal\iconset\IconsetInterface $iconset */
          $iconset = $this->iconsetManager->getInstance($icon['iconset']);

          $options['decorative'] = FALSE;
          $options['alt'] = $title;

          $build = $iconset->build($icon['icon'], $options);
          $build['#attributes'] = isset($build['#attributes'])
            ? AttributeHelper::mergeCollections($build['#attributes'], $attributes)
            : $attributes;

          $item['title'] = $this->renderer->renderPlain($build);
          $item['has_icon'] = TRUE;
        }
        catch (PluginException | PluginNotFoundException | IconNotFoundException $e) {
          // Don't render any icons if unable to fetch the requested iconset.
        }
      }

      if ($item['below']) {
        $this->formatIconOnly($item['below'], $options, $attributes);
      }
    }
  }

}
