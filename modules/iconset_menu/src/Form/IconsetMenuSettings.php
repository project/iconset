<?php

namespace Drupal\iconset_menu\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\system\Entity\Menu;
use Drupal\iconset\IconsetManagerInterface;
use Drupal\iconset_menu\IconsetMenuFormatter;

/**
 * Configuration form for configuring menu icon rendering settings.
 */
class IconsetMenuSettings extends FormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Utility for formatting menu with icon styles applied.
   *
   * @var \Drupal\iconset_menu\IconsetMenuFormatter
   */
  protected $menuFormatter;

  /**
   * The iconset manager.
   *
   * @var \Drupal\iconset\IconsetManagerInterface
   */
  protected $iconsetManager;

  /**
   * Create a new instance of the IconsetMenuSettings form class.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\iconset_menu\IconsetMenuFormatter $iconset_menu_formatter
   *   Utility for formatting menu with icon styles applied.
   * @param \Drupal\iconset\IconsetManagerInterface $iconset_manager
   *   The iconset manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, IconsetMenuFormatter $iconset_menu_formatter, IconsetManagerInterface $iconset_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->menuFormatter = $iconset_menu_formatter;
    $this->iconsetManager = $iconset_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('iconset.menu_formatter'),
      $container->get('strategy.manager.iconset')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'iconset_menu_configure_menu_icons';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Menu $menu = NULL) {
    if (!$menu) {
      $form['#access'] = FALSE;
      return $form;
    }

    $form_state->setTemporaryValue('menu_name', $menu->id());
    $defaults = $this->menuFormatter->getMenuSettings($menu);

    $styleOpts = [];
    foreach ($this->menuFormatter->getStyles() as $key => $format) {
      $styleOpts[$key] = $format['label'];
    }

    $form['icon_style'] = [
      '#type' => 'select',
      '#title' => $this->t('Menu render style'),
      '#options' => $styleOpts,
      '#default_value' => $defaults['icon_style'],
    ];

    $form['allowed_iconsets'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Available iconsets'),
      '#options' => $this->iconsetManager->getIconsets(),
      '#default_value' => $defaults['allowed_iconsets'],
    ];

    $form['width'] = [
      '#type' => 'number',
      '#title' => $this->t('Width (pixels)'),
      '#min' => 1,
      '#default_value' => $defaults['width'] ?? NULL,
      '#description' => $this->t('The width attribute to set for this icon, will be overridden by CSS, and will auto size to maintain aspect ratio if height is set and this is left blank.'),
    ];

    $form['height'] = [
      '#type' => 'number',
      '#title' => $this->t('Height (pixels)'),
      '#min' => 1,
      '#default_value' => $defaults['height'] ?? NULL,
      '#description' => $this->t('The height attribute to set for this icon, will be overridden by CSS, and will auto size to maintain aspect ratio if width is set and this is left blank.'),
    ];

    $form['css_class'] = [
      '#type' => 'css_class',
      '#title' => $this->t('Icon CSS classes'),
      '#default_value' => $defaults['css_class'],
      '#description' => $this->t('Additional CSS classes get added to each of the menu item icons.'),
    ];

    $form['actions'] = [
      '#type' => 'actions',

      'save' => [
        '#type' => 'submit',
        '#value' => $this->t('Save Icon Settings'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Currently not validations are needed.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($menuName = $form_state->getTemporaryValue('menu_name')) {
      /** @var \Drupal\system\MenuInterface */
      $menu = $this->entityTypeManager
        ->getStorage('menu')
        ->load($menuName);

      if ($menu) {
        $values = $form_state->getValues();
        $menu->setThirdPartySetting('iconset', 'icon_style', $values['icon_style']);
        $menu->setThirdPartySetting('iconset', 'allowed_iconsets', array_filter($values['allowed_iconsets']));
        $menu->setThirdPartySetting('iconset', 'css_class', $values['css_class']);

        foreach (['width', 'height'] as $dimension) {
          if (!empty($values[$dimension])) {
            $menu->setThirdPartySetting('iconset', $dimension, $values[$dimension]);
          }
          else {
            $menu->unsetThirdPartySetting('iconset', $dimension);
          }
        }

        $menu->save();

        $this
          ->messenger()
          ->addMessage($this->t('Menu icon settings successfully saved.'));
      }
    }
  }

}
