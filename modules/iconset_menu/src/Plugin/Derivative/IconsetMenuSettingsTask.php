<?php

namespace Drupal\iconset_menu\Plugin\Derivative;

use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\Routing\RouteProviderInterface;

/**
 * Optionally create menu local task based on if there are overriding routes.
 *
 * Often times we want to provider default menu tabs for entity list pages, but
 * will want to replace them with more advanced views functionality. Normally
 * this would create double the tabs.
 *
 * Using this deriver checks if the route proposed for the tab is overridden
 * and therefore doesn't need to be added.
 *
 * @todo Inspect views route to check for local task configuration to determine
 * if a local task should be added or ignored.
 */
class IconsetMenuSettingsTask extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The ID of the plugin the deriver is implementing.
   *
   * @var string
   */
  protected $basePluginId;

  /**
   * The route provider.
   *
   * @var \Drupal\Core\Routing\RouteProviderInterface
   */
  protected $routeProvider;

  /**
   * Create a new link object deriver.
   *
   * @param string $base_plugin_id
   *   The plugin ID of the deriver definition.
   * @param \Drupal\Core\Routing\RouteProviderInterface $route_provider
   *   The route provider.
   */
  public function __construct($base_plugin_id, RouteProviderInterface $route_provider) {
    $this->basePluginId = $base_plugin_id;
    $this->routeProvider = $route_provider;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $base_plugin_id,
      $container->get('router.route_provider')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives = [];

    try {
      $routeName = 'entity.menu.iconset_config';
      $route = $this->routeProvider->getRouteByName($routeName);

      if ($route) {
        $this->derivatives[$routeName] = [
          'id' => $routeName,
          'title' => 'Menu Icon Settings',
          'base_route' => 'entity.menu.edit_form',
          'route_name' => $routeName,
        ] + $base_plugin_definition;
      }
    }
    catch (RouteNotFoundException $e) {
      // Route doesn't exist, skip creating this tab.
    }

    return $this->derivatives;
  }

}
