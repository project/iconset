<?php

namespace Drupal\iconset_embed\Plugin\Filter;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Drupal\iconset\IconsetManagerInterface;
use Drupal\iconset\Exception\IconNotFoundException;

/**
 * Provides a filter to display embedded entities based on data attributes.
 *
 * @Filter(
 *   id = "iconset_icon",
 *   title = @Translation("Iconset embeds"),
 *   description = @Translation("Embeds icons from configured iconsets using data attributes: data-icon."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 *   settings = {
 *     "iconsets" = { },
 *   },
 *   weight = 20,
 * )
 */
class IconsetFilter extends FilterBase implements ContainerFactoryPluginInterface {

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The iconset plugin manager.
   *
   * @var \Drupal\iconset\IconsetManager
   */
  protected $iconsetManager;

  /**
   * Constructs a EntityEmbedFilter object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\iconset\IconsetManagerInterface $iconset_manager
   *   The entity type manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, IconsetManagerInterface $iconset_manager, RendererInterface $renderer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->iconsetManager = $iconset_manager;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('strategy.manager.iconset'),
      $container->get('renderer')
    );
  }

  /**
   * Callback for preg_match_callback() to replace found <drupal-iconset> tags.
   *
   * @param string[] $matches
   *   The tag <drupal-iconset> tag snippet and attributes which will get
   *   replaced. The first value will be the entire tag matched, and the second
   *   value are all the tag attributes, which need to be parsed.
   *
   * @return string
   *   A replacement string for <drupal-iconset> tag. This is either HTML
   *   representing the designated icon (data-icon attribute) or an empty
   *   string to just remove the icon placeholder.
   */
  protected function replacePlaceholder(array $matches) {
    if (preg_match_all('/\s*(data-icon|width|height)\s*="([^"]+)"/', $matches[1], $attrs, PREG_SET_ORDER)) {
      $allowedSets = $this->settings['iconsets'] ?? [];
      $options = [];

      foreach ($attrs as $attribute) {
        switch ($attribute[1]) {
          case 'data-icon':
            [$iconsetId, $iconId] = explode(':', $attribute[2]);
            break;

          case 'width':
          case 'height':
            $options[$attribute[1]] = intval($attribute[2]);
        }
      }

      if (empty($iconsetId) || empty($iconId)) {
        return '';
      }

      try {
        if (!empty($allowedSets) && !in_array($iconsetId, $allowedSets)) {
          // The icon was not in the allowed set of iconsets available, and
          // the DOM should just remove this DOM element and continue.
          throw new IconNotFoundException($iconsetId, $iconId);
        }

        /** @var \Drupal\iconset\IconsetInterface $iconset */
        $iconset = $this->iconsetManager->getInstance($iconsetId);
        $renderable = $iconset->build($iconId, $options);

        // Remove newline characters from the renderable result, as this causes
        // problems with text filters replacing them with <p> or <br> tags
        // and breaking the HTML output of the rendered icon.
        return preg_replace("#\r|\n#", '', $this->renderer->render($renderable));
      }
      catch (PluginNotFoundException | IconNotFoundException $e) {
        // Remove the placeholder, without rendering an icon.
      }
    }

    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    if ($long) {
      return $this->t('You can embed Iconset icons by using the HTML tag <drupal-iconset data-icon="{iconset}:{icon}" height="60" width="60"/> with the appropriate Iconset identifiers.');
    }
    else {
      return $this->t('You can embed Iconset icons.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    // Replace <drupal-iconset> HTML tags with icons or remove them if invalid
    // or restricted icons have been specified.
    $filtered = preg_replace_callback(
      '#\s*<drupal-iconset (.*?)(?:/>|>\s*</drupal-iconset>)\s*#im',
      [$this, 'replacePlaceholder'],
      $text
    );

    // Transform the DOM object back into plain HTML for replacement.
    $result = new FilterProcessResult($text);
    $result->setProcessedText($filtered);
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form['iconsets'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Allowed iconsets'),
      '#options' => $this->iconsetManager->getIconsets(),
      '#default_value' => $this->settings['iconsets'] ?? [],
      '#description' => $this->t('Leaving this setting as empty allows is the same as not restricting to any Iconsets.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $configuration['settings']['iconsets'] = array_filter($configuration['settings']['iconsets']);

    parent::setConfiguration($configuration);
  }

}
