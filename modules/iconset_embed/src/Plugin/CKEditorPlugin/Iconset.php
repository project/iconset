<?php

namespace Drupal\iconset_embed\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "iconset" plugin for inserting iconset icons with the CKEditor.
 *
 * @CKEditorPlugin(
 *   id = "drupaliconset",
 *   label = @Translation("Iconset icon"),
 *   module = "iconset",
 * )
 */
class Iconset extends CKEditorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return $this->getModulePath('iconset_embed') . '/js/ckeditor/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return [
      'core/jquery',
      'core/drupal',
      'core/drupal.ajax',
      'iconset/icon-selector',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    $assetPath = $this->getModulePath('iconset_embed') . '/assets/ckeditor/';

    return [
      'DrupalIconset' => [
        'label' => $this->t('Iconset'),
        'image' => $assetPath . 'iconset-btn.png',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [
      'DrupalIconset_dialogTitleAdd' => $this->t('Insert icon'),
      'DrupalIconset_dialogTitleEdit' => $this->t('Edit icon'),
      'DrupalIconset_icon' => $this->getButtons()['DrupalIconset']['image'],
    ];
  }

}
