<?php

namespace Drupal\iconset_embed\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\editor\EditorInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\editor\Ajax\EditorDialogSave;

/**
 * Create a form for editors to select and iconset icon to embed.
 */
class EditorIconSelector extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'iconset_editor_dialog_selector';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, EditorInterface $editor = NULL) {
    // The default values are set directly from \Drupal::request()->request,
    // provided by the editor plugin opening the dialog.
    $input = $form_state->getUserInput()['editor_object'] ?? [];
    $form['#tree'] = TRUE;
    $form['#attached']['library'][] = 'editor/drupal.editor.dialog';
    $form['#prefix'] = '<div id="editor-iconset-dialog-form">';
    $form['#suffix'] = '</div>';

    $form['icon'] = [
      '#type' => 'iconset_selector',
      '#title' => $this->t('Select icon'),
      '#required' => TRUE,
      '#default_value' => [
        'iconset' => $input['iconset'] ?? '',
        'icon' => $input['icon'] ?? '',
      ],
    ];

    /** @var \Drupal\filter\Plugin\FilterBase $iconFilter */
    $iconFilter = $editor->getFilterFormat()->filters()->get('iconset_icon');
    if ($iconFilter && $iconFilter->settings['iconsets']) {
      $form['icon']['#iconset'] = $iconFilter->settings['iconsets'];
    }

    $form['width'] = [
      '#type' => 'number',
      '#title' => $this->t('width'),
      '#min' => 1,
      '#step' => 1,
      '#default_value' => $input['width'] ?? '',
    ];

    $form['height'] = [
      '#type' => 'number',
      '#title' => $this->t('height'),
      '#min' => 1,
      '#step' => 1,
      '#default_value' => $input['height'] ?? '',
    ];

    $form['actions'] = [
      '#type' => 'action',

      // Submit only occurs using Javascript, and the WYSIWYG editor.
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Save'),
        '#submit' => [],
        '#ajax' => [
          'callback' => static::class . '::ajaxSubmitForm',
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Submission only happens with Javascript, and never uses this handler.
  }

  /**
   * AJAX callback for either reporting the error or triggering the editor save.
   *
   * @param array $form
   *   Reference to the form element structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form submission state, values and build information.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   A response with the AJAX actions commands to update the WYSIWYG editor.
   */
  public static function ajaxSubmitForm(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    if ($form_state->getErrors()) {
      unset($form['#prefix'], $form['#suffix']);

      $form['status_messages'] = [
        '#type' => 'status_messages',
        '#weight' => -10,
      ];
      $response->addCommand(new HtmlCommand('#editor-iconset-dialog-form', $form));
    }
    else {
      $values = [];
      foreach (['icon', 'height', 'width'] as $key) {
        $values[$key] = $form_state->getValue($key);
      }

      $response->addCommand(new EditorDialogSave($values));
      $response->addCommand(new CloseModalDialogCommand());
    }

    return $response;
  }

}
