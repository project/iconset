(({ Iconset, ckeditor }, CKEDITOR) => {
  /**
   * Add the Drupal iconset widget for CKEditor to add support for embedding icons.
   */
  CKEDITOR.plugins.add('drupaliconset', {
    requires: 'widget',

    beforeInit() {
      // Configure CKEditor DTD for custom drupal-entity element.
      // @see https://www.drupal.org/node/2448449#comment-9717735
      const { dtd } = CKEDITOR;

      dtd['drupal-iconset'] = { };

      // drupal-iconset tag is allowed where ever an IMG is allowed.
      Object.keys(dtd).forEach((tag) => {
        if (dtd[tag].img) {
          dtd[tag]['drupal-iconset'] = 1;
        }
      });

      dtd.a['drupal-iconset'] = 1;
    },

    init(editor) {
      // Register the iconset widget.
      editor.widgets.add('drupaliconset', {
        allowedContent: 'drupal-iconset[!data-icon,alt,height,width]{width,height}',
        requiredContent: 'drupal-iconset[data-icon]',

        data() {
          const [iconset, icon] = this.element.getAttribute('data-icon').split(':');
          const setData = Iconset.getLoaded(iconset);
          const setElement = (data) => {
            const info = data.get(icon);

            if (!(info && info.html)) return;

            this.element.setHtml(info.html);

            // Apply the attributes from the <drupal-iconset> tag to the icon.
            //
            // TODO: Update the icon rendering to be a callback which returns with the
            // width, height and icons. This allows other implementations and elements
            // which do not size using the width and height element attributes.
            const el = this.element.getFirst();
            const attrs = this.element.getAttributes();

            ['alt', 'width', 'height'].forEach((attr) => {
              if (attrs[attr]) {
                el.setAttribute(attr, attrs[attr]);
              }
            });
          };

          if (setData) {
            setElement(setData);
          }
          else {
            Iconset.getAsync(iconset).then(setElement);
          }
        },

        upcast(element, data) {
          const { attributes } = element;

          if (element.name !== 'drupal-iconset' || !attributes['data-icon']) {
            return;
          }

          data.attributes = CKEDITOR.tools.copy(attributes);
          data.link = null;

          if (element.parent.name === 'a') {
            data.link = CKEDITOR.tools.copy(element.parent.attributes);

            // Omit CKEditor-internal attributes.
            Object.keys(element.parent.attributes).forEach((attrName) => {
              if (attrName.indexOf('data-cke-') !== -1) {
                delete data.link[attrName];
              }
            });
          }

          return element;
        },

        downcast() {
          // eslint-disable-next-line new-cap
          let element = new CKEDITOR.htmlParser.element('drupal-iconset', this.data.attributes);

          if (this.data.link) {
            // eslint-disable-next-line new-cap
            const link = new CKEDITOR.htmlParser.element('a', this.data.link);
            link.add(element);
            element = link;
          }

          return element;
        },
      });

      // Command for creating icon selection dialog.
      editor.addCommand('editdrupaliconset', {
        allowedContent: 'drupal-iconset[!data-icon,alt,height,width]{width,height}',
        requiredContent: 'drupal-iconset[data-icon]',
        modes: { wysiwyg: 1 },
        canUndo: true,

        exec(instance) {
          const selected = instance.getSelection().getSelectedElement();
          const widget = selected ? instance.widgets.getByElement(selected, true) : null;
          let existing = {};

          const saveCallback = (values) => {
            const { iconset, icon } = values.icon;

            const el = instance.document.createElement('drupal-iconset');
            el.setAttribute('data-icon', `${iconset}:${icon}`);

            if (values.height) {
              el.setAttribute('height', values.height);
            }
            if (values.width) {
              el.setAttribute('width', values.width);
            }

            instance.insertHtml(el.getOuterHtml());
            instance.fire('saveSnapshot');
          };

          if (widget && widget.name === 'drupaliconset') {
            existing = { ...widget.data.attributes };

            [existing.iconset, existing.icon] = existing['data-icon'].split(':');
            delete existing['data-icon'];
          }

          ckeditor.openDialog(
            instance,
            Drupal.url(`admin/iconset/${instance.config.drupal.format}/dialog`),
            existing,
            saveCallback,
            {
              dialogClass: 'iconset-select-dialog',
              resizable: false,
            },
          );
        },
      });

      // Create the CKEditor button for editing or creating icons.
      editor.ui.addButton('DrupalIconset', {
        label: Drupal.t('Insert icon'),
        command: 'editdrupaliconset',
        icon: editor.config.DrupalIconset_icon,
        hidpi: true,
      });
    },

    afterInit(editor) {
      // If the Drupal link plugin is registered, allow this widget to be
      // wrapped in a link or anchor tag.
      if (editor.plugins.drupallink) {
        CKEDITOR.plugins.drupallink.registerLinkableWidget('drupaliconset');
      }
    },
  });
})(Drupal, CKEDITOR);
