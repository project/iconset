<?php

/**
 * @file
 * Iconset module global functions and Drupal module hooks.
 */

use Drupal\Core\Template\Attribute;

/**
 * Implements hook_theme().
 */
function iconset_theme($existing, $type, $theme, $path) {
  return [
    'iconset_svg_icon' => [
      'variables' => [
        'icon' => NULL,
        'attributes' => [],
      ],
    ],
    'iconset_font_icon' => [
      'variables' => [
        'tag' => 'i',
        'content' => NULL,
        'attributes' => [],
      ],
    ],
    'iconset_selector_wrapper' => [
      'render element' => 'element',
    ],
  ];
}

/**
 * Prepares variables for text format wrapper templates.
 *
 * Default template: iconset-selector-wrapper.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - attributes: An associative array containing properties of the element.
 */
function template_preprocess_iconset_selector_wrapper(array &$variables) {
  $element = &$variables['element'];
  $variables['children'] = $element['#children'];
  $variables['attributes']['id'] = $element['#id'];
  $variables['attributes']['role'] = 'group';
  $variables['aria_description'] = FALSE;

  if (!empty($element['#title_display'])) {
    $element += ['#title_display' => 'before'];
    $variables['label_display'] = $element['#title_display'];
    $variables['label'] = [
      '#theme' => 'form_element_label',
      '#title' => $element['#title'],
      '#required' => $element['#required'],
      '#title_display' => $element['#title_display'],
      '#attributes' => [
        'id' => $element['#id'] . '--label',
      ],
    ];

    $variables['attributes']['aria-labelledby'] = $element['#id'] . '--label';
  }

  if (!empty($element['#description'])) {
    $description_attributes = [];
    $variables['description_display'] = $element['#description_display'];

    if (!empty($element['#id'])) {
      $description_attributes['id'] = $element['#id'] . '--description';
    }
    $variables['description']['attributes'] = new Attribute($description_attributes);
    $variables['description']['content'] = $element['#description'];
  }

}
