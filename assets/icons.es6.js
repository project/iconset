(({ Toolshed, theme }) => {
  /**
   * Loading and rendering iconsets with Javascript.
   */
  Drupal.Iconset = {
    instances: new Map(),

    /**
     * Check and format a new iconset to the static iconset map.
     *
     * @param {array} key
     *   The machine identifier for this iconset.
     * @param {object} data
     *   Raw data object with information on how to render the new data.
     *
     * @return {Map|false}
     *   The processed iconset information in a Map (keyed by icon ID) or
     *   boolean FALSE if unable to build the icon data into a useable format.
     */
    processIconset(key, data) {
      if (this.handlers[data.type]) {
        return this.handlers[data.type](data);
      }

      return false;
    },

    /**
     * Create a new Promise to load an iconset from the iconset ID.
     *
     * @param {string} iconsetId
     *   The machine name of the iconset to load from the server.
     *
     * @return {Promise}
     *   Returns a promise for asynchronously loading the data for building
     *   additional iconsets for the selector to load.
     */
    fetchIconset(iconsetId) {
      return Toolshed.sendRequest(`ajax/iconset/${iconsetId}/info.json`).then(
        (json) => {
          const data = (typeof json === 'string' || json instanceof String) ? JSON.parse(json) : json;
          const iconset = this.processIconset(iconsetId, data);

          this.instances.set(iconsetId, iconset);
          return iconset;
        },
        (resp) => {
          this.iconsets.set(iconsetId, false);
          Error(`${resp.status}: ${resp.text}`);
        },
      );
    },

    /**
     * Check if the iconset has already been loaded, and is already available.
     *
     * this function does not next if iconsets can be loaded, only if they have
     * already been loaded.
     *
     * @param {string} id
     *   Machine name of the iconset check if it is already loaded.
     *
     * @return {bool}
     *   Returns true IFF the iconset has already been loaded to the clientside.
     */
    isLoaded(id) {
      const data = this.instances.get(id);
      return (data instanceof Map || data === false);
    },

    /**
     * Get loaded icon data if it is already available.
     *
     * @param {string} id
     *   Machine name of the iconset check to return if loaded.
     *
     * @return {array|false|null}
     *   Returns an array of the iconset icon data if a valid iconset is already
     *   loaded and processed. Will return boolean FALSE, if the iconset ID is
     *   invalid, or fails to load. NULL is returned if the data is not loaded
     *   or is in the progress of being requested.
     */
    getLoaded(id) {
      const data = this.instances.get(id);
      return (data instanceof Map || data === false) ? data : null;
    },

    /**
     * Fetchs the iconset data asynchronously if not already loaded. This method
     * handles sets which already are loaded, and sets that need to be loaded.
     *
     * Because the return is wrapped in a Promise even if the data is already
     * loaded and cached, there are circumstances where you may want to use
     * isLoaded() and getLoaded() to avoid FOUC or DOM manipulation issues.
     *
     * @param {string} id
     *   Machine name of the iconset to fetch, or return as the promise data.
     *
     * @return {Promise}
     *   Returns true IFF the iconset has already been loaded to the clientside.
     */
    getAsync(id) {
      const data = this.instances.get(id);

      if (data instanceof Map || data === false) {
        return Promise.resolve(data);
      }

      let promise = data;

      if (!data || typeof data.then !== 'function') {
        promise = this.fetchIconset(id);
        this.instances.set(id, promise);
      }

      return promise;
    },
  };

  /**
   * Iconset plugin handlers can register handlers to be used to process the
   * plugin handler's JSON data format into consistent icon data. This allows
   * different plugins to have different data formats and utilize different
   * theme functions in Javascript.
   *
   * All handlers are functions, that take information about a loaded asset,
   * as a JSON object, and return an array of icon object, each consisting of:
   *  - icon ID
   *  - label
   *  - string of HTML to preview the icon.
   */
  Drupal.Iconset.handlers = {

    /**
     * Function for converting the data from the iconset.
     *
     * @param {Object} data
     *   Iconset data about the iconset icons. At a minimum this data should
     *   have a "type" and "assets" properties. Assets is where information
     *   about the individual icons are contained.
     *
     * @return {Map}
     *   The Map object which was populated with the icons.
     */
    svg(data) {
      const icons = new Map();

      data.assets.forEach((asset) => {
        const renderer = (asset.type === 'sprite')
          ? theme.iconsetSvgSpriteItem
          : theme.iconsetImage;

        asset.icons.forEach((icon) => {
          icons.set(icon.id, {
            id: icon.id,
            label: icon.label,
            html: renderer({ ...icon, file: asset.file }),
          });
        });
      });

      return icons;
    },

    /**
     * Function for converting font icon data to renderable icons.
     *
     * @param {Object} data
     *   Data about a font icon, including the font properties, and individual
     *   data about font assets, and a map of unicode characters to icon IDs.
     *
     * @return {Map}
     *   The Map object which was populated with the icons.
     */
    font_icon(data) {
      const icons = new Map();

      data.assets.forEach((asset) => {
        const { tag } = data;
        const className = data.className.reduce((acc, curr) => `${acc} ${curr}`, '');

        asset.icons.forEach((icon) => {
          icons.set(icon.id, {
            id: icon.id,
            label: icon.label,
            html: theme.iconsetFontIcon(icon, tag, className),
          });
        });
      });

      return icons;
    },

    /**
     * Function for converting image file asset data to renderable icons.
     *
     * @param {Object} data
     *   Data about a font icon, including individual image files, dimensions,
     *   the image ID and label.
     *
     * @return {Map}
     *   The Map object which was populated with the icons.
     */
    image(data) {
      const icons = new Map();

      data.assets.forEach((icon) => {
        icons.set(icon.id, {
          id: icon.id,
          label: icon.label,
          html: theme.iconsetImage(icon),
        });
      });

      return icons;
    },
  };

  /**
   * Theme function to render SVG icon data from a sprite asset.
   *
   * @param {object} icon
   *   Information about the icon to render.
   * @param {object} opts
   *   Object with additional options like width and height to apply to the
   *   rendering of the icon.
   *
   * @return {string}
   *   The HTML content to use with as the icon display.
   */
  theme.iconsetSvgSpriteItem = (icon, opts = {}) => {
    let attrs = `class="icon icon--svg" alt="${icon.label}"`;

    if (icon.viewBox) {
      attrs += ` viewBox="${icon.viewBox}"`;
    }

    if (opts.height) {
      attrs += ` height="${opts.height}"`;
    }
    if (opts.width) {
      attrs += ` width="${opts.width}"`;
    }

    return `<svg ${attrs}>
      <use xlink:href="${encodeURI(icon.file)}#${encodeURIComponent(icon.id)}"/>
    </svg>`;
  };

  /**
   * Theme function to render image file into an iconset icon.
   *
   * @param {object} icon
   *   Information about the icon to render.
   * @param {object} opts
   *   Object with additional options like width and height to apply to the
   *   rendering of the icon.
   *
   * @return {string}
   *   The HTML content to use with as the icon display.
   */
  theme.iconsetImage = (icon, opts = {}) => {
    const height = opts.height || icon.height;
    const width = opts.width || icon.width;

    let attrs = `class="icon icon--img" alt="${icon.label}"`;

    if (height) {
      attrs += ` height="${height}"`;
    }
    if (width) {
      attrs += ` width="${width}"`;
    }

    return `<img ${attrs} src="${encodeURI(icon.file)}"/>`;
  };

  /**
   * Theme function to render SVG icon data from iconset.
   *
   * @param {object} icon
   *   Information about the icon to render.
   * @param {string} tag
   *   The HTML tag to use as the icon element.
   * @param {string} className
   *   CSS class name string to apply for the font icon element wrapper. Most
   *   font based icons use css class names to apply the font styles so that
   *   the content renders using the correct font-face and styles.
   * @param {object} opts
   *   Object with additional options like width and height to apply to the
   *   rendering of the icon.
   *
   * @return {string}
   *   The HTML content to use with as the icon display.
   */
  theme.iconsetFontIcon = (icon, tag, className = '', opts = {}) => {
    let attrs = `class="${`${className} icon icon--font`.trim()}" role="img" alt="${icon.label}"`;

    const dim = opts.height || opts.width;
    if (dim) {
      attrs += ` style="font-size: ${dim}px;"`;
    }

    return `<${tag} ${attrs}>${decodeURIComponent(icon.unicode)}</${tag}>`;
  };
})(Drupal);
