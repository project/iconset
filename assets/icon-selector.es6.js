(({
  t,
  behaviors,
  Toolshed: ts,
  Iconset,
}) => {
  /**
   * A widget for selecting an icon from an iconset.
   */
  class IconSelector {
    /**
     * Create a new icon selector for the icon selection widget.
     *
     * @param {HTMLElement} iconsetSel
     *   The select form input used to provide the value of the iconset.
     * @param {HTMLElement} iconSel
     *   The select form element that is linked to the icon selection.
     */
    constructor(iconsetSel, iconSel) {
      this.iconsetSel = iconsetSel;
      this.iconSel = new ts.FormElement(iconSel, { style: { display: 'none' } });
      this.ctrl = new ts.Element('a', { href: '#', class: 'iconset-selector__button' });

      this.selPane = new ts.AttachedPane(this.ctrl.el, {
        classes: ['iconset-selector__icons'],
        onRemoveItem: (item) => {
          item.removeEventListener('click', this.onIconClick);
        },
      });

      this.required = !!this.iconSel.getAttr('required');
      this.ctrl.attachTo(this.iconSel, 'after');
      this.ctrl.on('click', (e) => {
        e.preventDefault();

        if (this.iconset || this.selPane.isVisible()) {
          this.selPane.toggle();
        }
      });

      this.iconset = '';
      if (this.iconsetSel.value) {
        this.setIconset(this.iconsetSel.value);
      }

      // Attach the event listeners for select, interactions.
      if (this.iconsetSel.tagName === 'SELECT') {
        this.onIconsetChange = () => {
          this.setIconset(this.iconsetSel.value);
        };

        this.iconsetSel.addEventListener('change', this.onIconsetChange);
      }
    }

    /**
     * Clean-up any resources and remove event listeners.
     */
    destroy() {
      if (!this.init) return;

      this.iconSel.style.display = '';
      this.ctrl.destroy(true);
      this.selPane.destroy();

      if (this.onIconsetChange) {
        this.iconsetSel.removeEventListener('change', this.onIconsetChange);
      }

      delete this.selPane;
      delete this.ctrl;
    }

    /**
     * Change the current set of icons to a new set.
     *
     * @param {string} id
     *   Name of the iconset to load as the select widget.
     */
    setIconset(id) {
      // Already on this iconset, no need to update or change anything.
      if (this.iconset === id) return;

      const icons = Iconset.getLoaded(id);

      // Check if icons have already been loaded, or need to be fetched.
      if (Array.isArray(icons)) {
        this.setIconsetData(id, icons);
      }
      else if (icons === false) {
        this.selPane.close();
        this.iconset = '';
      }
      else {
        this.selPane.clear();
        Iconset.getAsync(id).then((data) => this.setIconsetData(id, data));
      }
    }

    /**
     * Internal function for setting the icons in the dialog wrapper.
     *
     * @param {string} iconset
     *   Machine name of the iconset being set.
     * @param {array} data
     *   The icon data as an array of icon info with a `html` property.
     *
     * @return {array}
     *   Return the data passed in unmodified to allow chaining with a Promise
     *   if multiple selectors add then() callbacks when waiting for the loading
     *   to complete.
     */
    setIconsetData(iconset, data) {
      if (this.iconset !== iconset) {
        this.iconset = iconset;
        this.selPane.clear();

        if (data) {
          if (!this.required) {
            // Prepend and empty icon selection when an icon is not required.
            const el = this.createIconSelector({ id: '', label: t('None'), html: '' });
            this.selPane.append(el);
          }

          data.forEach((icon) => {
            const el = this.createIconSelector(icon);
            this.selPane.append(el);
          });
        }

        // Icon does not exist in the new set, clear the value
        const icon = this.iconSel.value;
        if (!icon || !data.has(icon)) {
          this.iconSel.value = '';
          this.ctrl.innerHTML = '';
        }
      }

      return data;
    }

    /**
     * Creates a new button which selects an icon.
     *
     * @param {Object} icon
     *   Icon data, which contains the HTML, and icon ID.
     *
     * @return { ToolshedElement }
     *   Element wrapper which has information about the generated icon display.
     */
    createIconSelector(icon) {
      const el = new ts.Element('a', { href: '#', class: 'icon-selector' });
      el.innerHTML = icon.html;
      el.iconData = icon;

      if (icon.id === this.iconSel.value) {
        el.addClass('icon--active');
        this.ctrl.innerHTML = icon.html;
      }

      el.on('click', (e) => {
        e.preventDefault();
        this.selectIcon(el);
      }, true);

      return el;
    }

    /**
     * Event handler for when an icon selection is clicked.
     *
     * @param {ToolsheElement} item
     *   The icon data element for the icon being selected.
     */
    selectIcon(item) {
      this.selPane.close();
      this.iconSel.value = item.iconData.id;
      this.ctrl.innerHTML = item.iconData.html;

      ts.walkByClass(this.selPane.content.el, 'icon--active', (el) => {
        el.classList.remove('icon--active');
      });

      item.addClass('icon--active');
    }
  }

  /**
   * Create a global tracking for loaded data of iconsets.
   *
   * The map keys are the iconset IDs and the values are data objects provide
   * the information on how the icons are rendered.
   *
   * @type {Map}
   */
  IconSelector.iconsets = new Map();

  /**
   * Icon selector behavior, to find and apply JS to icon selector widgets.
   */
  behaviors.iconsetSelector = {
    selectors: new Map(),

    attach(context) {
      ts.walkByClass(context, 'js-iconset-selector', (el) => {
        const iconSelector = new IconSelector(
          el.getElementsByClassName('icon-sel__iconset')[0],
          el.getElementsByClassName('icon-sel__icon')[0],
        );

        this.selectors.set(el.id, iconSelector);
      }, 'iconset-selector--processed');
    },
    detach(context, settings, trigger) {
      if (trigger !== 'unload') return;

      ts.walkByClass(context, '.js-iconset-selector', (el) => {
        const iconSel = this.selectors.get(el.id);

        if (iconSel) {
          this.selectors.delete(el.id);
          iconSel.destroy();
        }
      });
    },
  };
})(Drupal);
