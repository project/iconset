# Iconset

Provides functionality for making sets of icons available to Drupal. The types
of icons it can support is extendable and ships with icon handlers for working
with SVG symbols (sprites) and glyphs (font).

The base module relies on other modules and themes to declare the iconsets to
include in a `<extension>.iconset.yml` file. Other modules can extend how
iconsets become available (i.e. `iconset_custom`).

Once `Iconset` is aware of your icons, they will be available through form
selector elements and fields.


## Defining Iconset

The core module allows modules and themes to provide iconsets using a YAML file
which provide information about what assets belong to an iconset and which
icon handler plugin should be used to handle that asset.

`Iconset` ships with two icon handlers, one for SVG (`svg`) files and another
for SVG font files (`svg_font` still in development). Other handlers can be
added by implementing the `\Drupal\iconset\Plugin\IconHandlerInterface` and
using the `@IconsetIconHandler` annotation.

The handler's plugin ID should be prepended to each asset as it is listed. More
information on how each individual `Iconset` icon handler works can be found
for each individual handler's class definition comments.

Example: example.iconset.yml
```
# An Iconset with two SVG files.
fontawesome_brands:
  label: 'Fontawesome-Brands'
  plugin: 'svg'
  assets:
    - '/libraries/fontawesome/sprites'  # directory
    - 'assets/icons/brands.svg'         # Single SVG file

# Icons available from font files, but defined in an SVG format.
fontawesome_regular:
  label: 'Fontawesome-Regular'
  plugin: 'svg_font'
  config:
    tag: i
    css_class:
      - 'far'
    font:
      family: 'Font Awesome 5 Free'
      weight: 400
  assets:
    - '/libraries/fontawesome/webfonts/fa-regular-400.svg'
```

Optionally a icon builder can be specified in the &gt;module&lt;.iconset.yml for
each of the iconset definitions to define a custom icon render element builder.

The builder should implement `\Drupal\iconset\IconBuilderInterface` and will be
called instead of the plugin handler when building the icon render array. This
allows full customization of the rendering output of an iconset.

```
# Iconset definition with a custom builder.
fontawesome_brands:
  label: 'Fontawesome-Brands'
  plugin: 'svg'
  builder: \Drupal\example\CustomIconBuilder
  assets:
    - 'assets/icons/brands.svg'
```


## Icon rendering

The easiest way to render an icon is to use the `iconset_icon` render element.

Example:
```
$build['example_icon'] = [
  '#type' => 'iconset_icon',
  '#iconset' => 'iconset',
  '#icon' => 'icon_id',
  '#width' => 40, _// Optional_
  '#height' => 40, _// optional_
  '#alt_text => t('Facebook'), _// Optional_
];
```

@see Drupal\iconset\Element\IconsetIcon

A Twig function is also available for use while working with twig templates.

Example:
```
{{ iconset_icon('iconset', 'icon_id', { width: 40, height: 40, class: [] }) }}
```

@see Drupal\iconset\Twig\IconsetTwigExtension


## Iconset selector

`Iconset` provides a form element for selecting icons from available iconsets.

The *#iconset* property is use to restricted the iconset which will be available
to select from.

Example:
```
$build['example_iconset_selector'] = [
  '#type' => 'iconset_selector',
  '#title' => $this->t('Select an icon'),
  '#iconset' => ['fontawesome_brands', 'fontawesome_regular'],
  '#default_value' => [
    'iconset' => 'fontawesome_brands',
    'icon' => 'facebook',
  ],
];
```

@see Drupal\iconset\Element\IconsetSelector


## Extending
